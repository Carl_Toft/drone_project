// Functions for bundle adjustment on single camera pose only, given an initial camera pose and a set of 2D-3D correspondences
#include <Eigen/Eigen>
#include <unsupported/Eigen/MatrixFunctions>  // for matrix exponential

// Calculates residual vector for single camera bundle adjustment problem
// The residuals are arranged in the same manner as for CalculateJacobian below,
// that is, r1x, r1y, r2x, r2y, etc.
//
// Inputs: P                REAL [3x4]
//                          Camera matrix
//         X                REAL [3xN]
//                          3D space points
//         x                REAL [2xN] or [3xN]
//                          Image points. If they are supplied as a homogenous vector
//                          the third coordinate must be 1, i.e., the first two elements
//                          must be the metric coordinates
//
// Outputs: oResiduals      REAL [2*N x 1]
//                          Residual vector
void CalculateResidualVector(Eigen::MatrixXd& P, Eigen::MatrixXd& X, Eigen::MatrixXd& x, Eigen::MatrixXd& oResiduals)
{
    int N = X.cols();
    oResiduals.resize(2*N, 1);
    Eigen::Matrix3d R = P.block(0,0,3,3);
    Eigen::Vector3d t = P.block(0,3,3,1);

    for (int j = 0; j < N; j++) {
        double denom = R.row(2)*X.col(j)+t(2,0);

        oResiduals(2*j) = (R.row(0)*X.col(j)+t(0,0))/denom - x(0,j);
        oResiduals(2*j+1)=(R.row(1)*X.col(j)+t(1,0))/denom - x(1,j);
    }
}

// Calculates the Jacobian for single camera bundle adjustment. The jacobian are with respect to the translational
// degrees of freedom and the three rotational degrees of freedom in exponential coordinates. So the variables
// are listed as (t1, t2, t3, a1, a2, a3), and the residuals are listed as (r1x, r1y, r2x, r2y, ...., rNx, rNy),
// where rix denotes the residual of the x-component of 2D-3D correspondence i.
//
// Inputs: P:           REAL [3x4]
//                      Camera matrix
//         X:           REAL [3xN]
//                      3D points
//
// Outputs: oJacobian   REAL [2*N x 6]
//                      Jacobian matrix for the residual vector
//
void CalculateJacobian(Eigen::MatrixXd& P, Eigen::MatrixXd& X, Eigen::MatrixXd& oJacobian)
{
    int N = X.cols();
    oJacobian.resize(2*N, 6);
    Eigen::Matrix3d R = P.block(0,0,3,3);
    Eigen::Vector3d t = P.block(0,3,3,1);

    Eigen::Matrix3d S1, S2, S3;
    S1 << 0.0, -1.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0;
    S2 << 0.0, 0.0, -1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0;
    S3 << 0.0, 0.0, 0.0, 0.0, 0.0, -1.0, 0.0, 1.0, 0.0;

    Eigen::Matrix3d S1R = S1*R;
    Eigen::Matrix3d S2R = S2*R;
    Eigen::Matrix3d S3R = S3*R;
    for (int j = 0; j < N; j++) {
        for (int residual = 0; residual < 1; residual++) {
            double denom = R.row(2) * X.col(j) + t(2, 0);
            double nom = R.row(residual) * X.col(j) + t(residual, 0);

            // Calculates derivatives of translational degrees of freedom
            oJacobian(2 * j + residual, 0) = 1.0 / denom;
            oJacobian(2 * j + residual, 1) = 0.0;
            oJacobian(2 * j + residual, 2) = -nom / (denom * denom);

            // Calculate derivatives of rotational degrees of freedom
            double product1 = S1R.row(residual) * X.col(j), product2 = S1R.row(2)*X.col(j);
            oJacobian(2 * j + residual, 3) = (product1*denom - product2*nom) / (denom * denom);
            product1 = S2R.row(residual)*X.col(j); product2 = S2R.row(2)*X.col(j);
            oJacobian(2 * j + residual, 4) = (product1*denom - product2*nom) / (denom * denom);
            product1 = S2R.row(residual)*X.col(j); product2 = S3R.row(2)*X.col(j);
            oJacobian(2 * j + residual, 5) = (product1*denom - product2*nom) / (denom * denom);
        }
    }
}

// Applies the step h obtained from the Levenberg-Marquardt method to the camera matrix P and returns it
// as a new camera matrix
Eigen::MatrixXd ApplyLevMarqStep(const Eigen::MatrixXd& P, Eigen::MatrixXd h)
{
    Eigen::MatrixXd R = P.block(0,0,3,3);
    Eigen::Vector3d t = P.block(0,3,3,1);

    for (int i = 0; i < 3; i++)
        t(i,0) += h(i,0);

    Eigen::MatrixXd A(3,3);
    A << 0, -h(3,0), -h(4,0), h(3,0), 0, -h(5,0), h(4,0), h(5,0), 0;
    R = A.exp()*R;

    Eigen::MatrixXd newP(3,4);
    newP << R, t;

    return newP;
}

// Calculates the step h in a Levenberg-Marquardt iteration by solving the equations
// (J^T*J + lambda*I)*h = -J^T*r.
Eigen::VectorXd CalculateLevMarqStep(Eigen::MatrixXd& jacobian, Eigen::MatrixXd& residuals, double lambda)
{
    // Set up linear system
    Eigen::MatrixXd LHS = jacobian.transpose()*jacobian + lambda*Eigen::MatrixXd::Identity(6,6);
    Eigen::MatrixXd RHS = -jacobian.transpose()*residuals;

    Eigen::VectorXd step = LHS.colPivHouseholderQr().solve(RHS);

    return step;
}

// Takes as input a current estimate of the camera matrix, the 3D space points X and the image points
// x, and returns a new camera matrix obtained by performing a single Levenberg-Marquardt step.
// The image and space points should be supplied as 2xN and 3xN matrices, respectively, where N is the
// number of points included in the optimization.
Eigen::MatrixXd PerformLevMarqStep(Eigen::MatrixXd& P, Eigen::MatrixXd& X, Eigen::MatrixXd x, double lambda)
{
    Eigen::MatrixXd res, J; // residual and Jacobian matrix
    CalculateJacobian(P,X,J);
    CalculateResidualVector(P,X,x,res);
    Eigen::VectorXd h = CalculateLevMarqStep(J, res, lambda);
    Eigen::MatrixXd P_updated = ApplyLevMarqStep(P, h);

    return P_updated;
}
