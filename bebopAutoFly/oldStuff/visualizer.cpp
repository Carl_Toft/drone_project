
#include "visualizer.h"
#include "global.h"
#include <curses.h>
#include <pthread.h>
#include <string>
#include <fstream>

Visualizer::Visualizer(){
    threadCreated = false;
    keybindings = "emergency: e, quit: q, takeOff: t, land: space, altitude & heading: arrow keys, translation: wasd, take picture: p, download picture: l, clear memory: o, manImProc: k";
    lastKey = ' ';
    batteryLevel = 0;
    numInliers = 0;
    dataDir = "data/";
    filename = "dronePos.tsv";
    debugNum = 0;
    debugText = "no break yet";
}

// to be run continuously in a separate thread
void Visualizer::printToTerminal(){
    int line;
    const int lineStart = 2;
    const int lineDist = 3;
    
    while(mainLoopRunning){
        sem_wait(&sem_printTerminal);
        line = lineStart;
        printKeybindings        ( line += lineDist );
        printBatteryLevel       ( line += lineDist );
        printIntegratedPosition ( line += lineDist );
        printNumInliers         ( line += lineDist );
        printLastKeyStroke      ( line += lineDist );
        printDebug              ( line += lineDist );
        writePoseToFile();
        usleep(10);
    }
    pthread_exit(NULL);
}

// helper function to make a thread use a member-function
// void Visualizer::*printToTerminal_helper(void *context){
//     ((Visualizer *) context)->printToTerminal();
//     return NULL;
// }

int Visualizer::setup(){
    sem_init(&sem_printTerminal, 0, 1);
    pthread_create(&thread, NULL, &printToTerminal_helper, (void *)this);
    threadCreated = true;
    return 0;
}

void Visualizer::cleanup(){
    mainLoopRunning = false;
    sem_post(&sem_printTerminal);
    if (threadCreated){
        pthread_join(thread, NULL);
    }
    sem_destroy(&sem_printTerminal);
}

void Visualizer::updateBatteryLevel(double bl){
    batteryLevel = bl;
    sem_post(&sem_printTerminal);
}

void Visualizer::updateLastKey(char k){
    lastKey = k;
    sem_post(&sem_printTerminal);
}

void Visualizer::updateNumInliers(int ni){
    numInliers = ni;
    sem_post(&sem_printTerminal);
}

void Visualizer::updatePose(Pose& newPose){
    pose.updatePose(newPose);
    sem_post(&sem_printTerminal);
}

void Visualizer::updateDebug(std::string message){
    debugNum++;
    debugText = message;
    sem_post(&sem_printTerminal);
}

void Visualizer::printKeybindings(int line){
    move(line, 0);  // move to begining of line
    clrtoeol();     // clear line
    mvprintw(line, 0, "KEYBINDINGS: %s", keybindings.c_str());
}

void Visualizer::printBatteryLevel(int line){
    move(line, 0);  // move to begining of line
    clrtoeol();     // clear line
    mvprintw(line, 0, "BATTERY: %f", batteryLevel);
}

void Visualizer::printLastKeyStroke(int line){
    move(line, 0);  // move to begining of line
    clrtoeol();     // clear line
    mvprintw(line, 0, "LAST KEY: %c", lastKey);
}

void Visualizer::printIntegratedPosition(int line){
    move(line, 0);  // move to begining of line
    clrtoeol();     // clear line
    mvprintw(line, 0, "INTEGRATED POSITION (x|y|z): %f | %f | %f", pose.x, pose.y, pose.z);
}

void Visualizer::printNumInliers(int line){
    move(line, 0);  // move to begining of line
    clrtoeol();     // clear line
    mvprintw(line, 0, "Number of inliers: %i", numInliers);
}

void Visualizer::printDebug(int line){
    move(line, 0);  // move to begining of line
    clrtoeol();     // clear line
    mvprintw(line, 0, "Debug: %i, %s", debugNum, debugText.c_str());
}

void Visualizer::writePoseToFile(){
    file.open(dataDir + filename);
    file << pose.x << "\t" << pose.y << "\t" << pose.z << pose.angle << std::endl;
    file.close();
}