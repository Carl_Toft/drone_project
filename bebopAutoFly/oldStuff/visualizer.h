#ifndef _VISUALIZER
#define _VISUALIZER

#include "misc.h"
#include <pthread.h>
#include <semaphore.h>
#include <string>
#include <fstream>

class Visualizer{
    private:
        
        std::string keybindings;
        char lastKey;
        double batteryLevel; // percent
        int numInliers;
        Pose pose;
        int debugNum;
        std::string debugText;
        
        std::ofstream file;
        std::string dataDir;
        std::string filename;
        
        pthread_t thread;
        sem_t sem_printTerminal;
        bool threadCreated;
        
        void printToTerminal(); // to be run continuously in a separate thread
        
        static void *printToTerminal_helper(void *context){ // helper function to make a thread use a member-function
            ((Visualizer *) context)->printToTerminal();
            return NULL;
        }
        
        void printKeybindings(int line);
        void printBatteryLevel(int line);
        void printIntegratedPosition(int line);
        void printLastKeyStroke(int line);
        void printNumInliers(int line);
        void printDebug(int line);
        
        void writePoseToFile();
        
    public:
        Visualizer();
        //~Visualizer();
        
        int setup();
        void cleanup();
        
        void updateBatteryLevel(double bl);
        void updateLastKey(char k);
        void updateNumInliers(int ni);
        void updatePose(Pose &newPose);
        void updateDebug(std::string message);
};

#endif