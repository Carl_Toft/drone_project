#ifndef _DRONE_CONTROLLER
#define _DRONE_CONTROLLER

#include "locator.h"
#include "navigator.h"
#include "userInterface.h"
#include "global.h"
#include <pthread.h>
#include <semaphore.h>
#include <time.h>
#include <string>

extern "C"{
    #include <libARController/ARController.h>
    #include <libARSAL/ARSAL.h>
}

// Forward declarations
class Navigator;
class userInterface;
class Locator;

enum DRONE_COMMANDS_t {
    DRONE_COMMAND_EMERGENCY,
    DRONE_COMMAND_STOP,
    DRONE_COMMAND_TAKEOFF,
    DRONE_COMMAND_LAND,
    DRONE_COMMAND_TAKE_IMAGE,
    DRONE_COMMAND_DOWNLOAD_IMAGE,
    DRONE_COMMAND_TAKE_AND_DOWNLOAD_IMAGE,
    DRONE_COMMAND_TOGGLE_VIDEO_CAPTURE,
    DRONE_COMMAND_DOWNLOAD_ALL,
    DRONE_COMMAND_CLEAR_MEMORY,
    DRONE_COMMAND_MOVE,
    DRONE_COMMAND_TURN,
    DRONE_COMMAND_MOVE_BY,
    DRONE_COMMAND_TURN_BY,
    DRONE_COMMAND_DO_NOTHING
};

class DroneController{
    private:
        Locator * locator;
        Navigator * navigator;
        UserInterface * userInterface;
        
        bool videoCaptureOn;
        static bool firstTimer;
        static time_t timer;
        
        ARCONTROLLER_Device_t *deviceController;
        sem_t sem_droneControllerState;
        sem_t sem_takeImage;
        sem_t sem_activateVideoCapture;
        pthread_mutex_t mutex_takeImage;
        
        static void *downloadAll_func(void * arg);
        
        void emergency();
        void stop();
        void takeOff();
        void land();
        int takeImage();
        void downloadImage(std::string dest);
        int takeAndDownloadImage(std::string dest);
        void toggleVideoCapture();
        void downloadAll(std::string dest);
        void clearMemory();
        void move(double x, double y, double z);
        void turn(double angle);
        void moveBy(double x, double y, double z);
        void turnBy(double angle);
        void roll(double x);
        void pitch(double y);
        void gaz(double z);
        
    public:
        DroneController();
        //~DroneController();
        
        void setLocator(Locator * l);
        void setNavigator(Navigator * n);
        void setUserInterface(UserInterface * u);
        
        int start();
        void close();
        
        static void droneEventCallback(eARCONTROLLER_DICTIONARY_KEY commandKey, ARCONTROLLER_DICTIONARY_ELEMENT_t *elementDictionary, void *customData);
        static void controllerStateCallback(eARCONTROLLER_DEVICE_STATE newState, eARCONTROLLER_ERROR error, void *customData);
        
        // Using only one callback-function helps making a soft interface and improve modularity
        int commandCallback(DRONE_COMMANDS_t command);
        int commandCallback(DRONE_COMMANDS_t command, double angle);
        int commandCallback(DRONE_COMMANDS_t command, std::string dest);
        int commandCallback(DRONE_COMMANDS_t command, double x, double y, double z);
};

#endif