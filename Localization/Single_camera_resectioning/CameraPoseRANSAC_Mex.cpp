#include "p3p.h"
#include <mex.h>
#include <Eigen/Eigen>
#include <unsupported/Eigen/MatrixFunctions>  // for matrix exponential

Eigen::MatrixXd cameraPoseRANSAC(const Eigen::MatrixXd& impoints, const Eigen::MatrixXd& spacePoints, unsigned int numIterations, double tol, unsigned int& oNumInliers);
Eigen::MatrixXd PerformLevMarqStep(Eigen::MatrixXd& P, Eigen::MatrixXd& X, Eigen::MatrixXd x, double lambda);

// Takes an mxArray and returns its contents as an eigen matrix by reference
void input_matrix(const mxArray* p, Eigen::MatrixXd& mat) {
    int m = mxGetM(p);
    int n = mxGetN(p);

    mat.resize(m,n);

    double *data = mxGetPr(p);

    for(int j = 0; j < n; j++) {
        for(int i = 0; i < m; i++) {
            mat(i,j) = data[m*j + i];
        }
    }
}

// Should be called as:
// [P, inliers] = CameraPoseRANSAC_Mex(X,x,tol), where X is a 3xN matrix containing the 3D points, and
// x is a 3xN matrix containing the image points as unit vectors and tol is the largest allowed
// angular deviation (in radians) for a correspondence to be considered an inlier
// The output P is the calculated camera matrix as well as an integer designating the number of inliers.
void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
    // Validate the input
    if(nrhs != 3)
        mexErrMsgTxt("Invalid input! The function takes three inputs: 3d structure X as a 3xN matrix, a 3xN matrix representing image points as unit vectors, as well as the maximum allowed angle deviation for a correspondence to be considered an inlier");
    if(nlhs != 2)
        mexErrMsgTxt("Invalid output! The function returns two outputs: P representing the triangulated camera matrix and an integer designating the number of inliers for P. ");

    Eigen::MatrixXd x;
    Eigen::MatrixXd X;
    double tol = mxGetScalar(prhs[2]);

    input_matrix(prhs[0],X);
    input_matrix(prhs[1],x);

    if(x.rows() != 3 || X.rows() != 3 || x.cols() != X.cols())
        mexErrMsgTxt("Input matrices must both have three rows and the same number of columns!");

    // Inputs and outputs are ok! Calculate camera pose!
    unsigned int numInliers = 0;
    Eigen::MatrixXd P = cameraPoseRANSAC(x,X,300,tol,numInliers);

    // Create output
    plhs[0] = mxCreateDoubleMatrix(3,4,mxREAL);
    double* outputMatrix = mxGetPr(plhs[0]);
    int index = 0;
    for (int j = 0; j < 4; j++) {
        for (int i = 0; i < 3; i++) {
            outputMatrix[index] = P(i,j);
            index++;
        }
    }

    plhs[1] = mxCreateDoubleMatrix(1,1,mxREAL);
    double* output = mxGetPr(plhs[1]);
    output[0] = static_cast<double>(numInliers);
}
