run /home/mattias/programs/cvx/cvx_startup.m
addpath(genpath('/home/mattias/mosek'))
addpath('/home/mattias/programs/SfM')
addpath('/home/mattias/programs/SfM/SfM_lib')
addpath(genpath('/home/mattias/programs/SfM/siftDemoV4'))
addpath(genpath('/home/mattias/programs/SfM/visionary'))
%addpath(genpath('/home/mattias/programs/SfM/vlfeat-0.9.13'))
addpath(genpath('/home/mattias/programs/vlfeat-0.9.20'))
%addpath(genpath('/home/mattias/programs/TOOLBOX_calib'))
