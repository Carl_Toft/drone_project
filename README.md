Repository for drone visual navigation project. The code requires the Eigen matrix algebra library to run. If you don't have it installed and are using Ubuntu, you can install it by typing 

sudo apt-get install libeigen3-dev

in the terminal. 

To compile the 3pt RANSAC code, type 

g++ -I/usr/include/eigen3 main.cpp p3p.cpp -std=c++11 -o ransac3pt

in the terminal. 

The code uses the 3pt solver by Laurent Kneip in his monocular pose estimation library which can be found here

https://github.com/uzh-rpg/rpg_monocular_pose_estimator