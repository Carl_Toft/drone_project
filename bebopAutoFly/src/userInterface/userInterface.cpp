
#include "userInterface.h"
#include "navigator.h"
#include "locator.h"
#include "global.h"
#include <pthread.h>
#include <string>
#include <fstream>
#include <iostream>
#include <allegro5/allegro.h>
#include <allegro5/allegro_image.h>
#include <allegro5/allegro_font.h>
#include <allegro5/allegro_ttf.h>
#include <ctime>
#include <sys/stat.h>

UserInterface::UserInterface(){
    (*this).droneController = NULL;
    navigator = NULL;
    locator = NULL;
//     pathPlanner = NULL;
    
    lastKey = ' ';
    batteryLevel = 0;
    numInliers = 0;
    videoState = false;
    continuousImageLocalizationState = false;
//     navigationState = NAVIGATOR_SCHEME_NONE;
    navigationState = 1;
    
    manualControlState = true;
    pilotingValue = 50;
    nbrOfMoveButtonsDown = 0;
    
    display = NULL;
    image = NULL;
    event_queue = NULL;
    timer = NULL;
    font = NULL;
    
    redraw = true;
    threadCreated = false;
    lastImageUpdateTime = time(NULL);
    
    dataDir = "data/";
    filename = "dronePos.tsv";
    imageName = "lastImage.jpg";
    imageNameManual = "manualImage.jpg";
    fontName = "HighlandGothicFLF.ttf";
    
    keybindings = "emergency: e, quit: q, takeOff: t, land: space, altitude & heading: arrow keys, translation: wasd, toggle video capture: o take picture: p, clear memory: k, download all: l, move to waypoint: m, save waypoint n, start calibraion dance: c, toggle image localization: h";
    FPS = 30;
    DISPLAY_WIDTH = 1600; // 800
    DISPLAY_HEIGHT = DISPLAY_WIDTH;
    BORDER_SIZE = 10;
    FONT_SIZE = DISPLAY_WIDTH/50;
}

void UserInterface::setDroneController(DroneController * d){
    (*this).droneController = d;
}

void UserInterface::setNavigator(Navigator * n){
    navigator = n;
}

void UserInterface::setLocator(Locator * l){
    locator = l;
}

// void UserInterface::setPathPlanner(PathPlanner * p){
//     (*this).pathPlanner = p;
// }

bool UserInterface::getManualControlState(){
    return manualControlState;
}

int UserInterface::start(){
    if((*this).droneController == NULL || navigator == NULL || locator == NULL){
        mainLoopRunning = false;
        return 1;
    }
    pthread_create(&thread, NULL, &run_helper, (void *)this);
    threadCreated = true;
    return 0;
}

void UserInterface::close(){
    mainLoopRunning = false;
    if (threadCreated){
        pthread_join(thread, NULL);
    }
}

void *UserInterface::run_helper(void *context){ // helper function to make a thread use a member-function
    ((UserInterface *) context)->run();
    return NULL;
}

// to be run continuously in a separate thread
void UserInterface::run(){
    if(setup()){
        std::cerr << "Error: Failed to setup allegro" << std::endl;
        mainLoopRunning = false;
        pthread_exit(NULL);
    }
    
    while(mainLoopRunning){
        ALLEGRO_EVENT ev;
        al_wait_for_event(event_queue, &ev);
        
        if(ev.type == ALLEGRO_EVENT_TIMER) {
            if(nbrOfMoveButtonsDown <= 0 && manualControlState){
                (*this).droneController->commandCallback(DRONE_COMMAND_STOP);
            }
            updateImage();
            redraw = true;
        }
        else if(ev.type == ALLEGRO_EVENT_DISPLAY_CLOSE) {
            mainLoopRunning = false;
            break;
        }
        else if(ev.type == ALLEGRO_EVENT_KEY_DOWN) {
            keyDownActions(ev);
        }
        else if(ev.type == ALLEGRO_EVENT_KEY_UP) {
            keyUpActions(ev);
        }
        if(redraw && al_is_event_queue_empty(event_queue)) {
            print();
        }
    }
    
    al_destroy_display(display);
    al_destroy_timer(timer);
    al_destroy_event_queue(event_queue);
    al_destroy_font(font);
    al_destroy_bitmap(image);
    
    pthread_exit(NULL);
}

int UserInterface::setup(){    
    //******************* INITIALIZE STUFF *********************
    if(!al_init()) {
        std::cerr << "Error: Failed to initialize allegro!" << std::endl;
        return 1;
    }
    
    if(!al_init_image_addon()) {
        std::cerr << "Error: Failed to initialize al_init_image_addon!" << std::endl;
        return 1;
    }
    
    if(!al_init_font_addon()) {
        std::cerr << "Error: Failed to initialize al_init_font_addon!" << std::endl;
        return 1;
    }
    
    if(!al_init_ttf_addon()) {
        std::cerr << "Error: Failed to initialize al_init_ttf_addon!" << std::endl;
        return 1;
    }
    
    if(!al_install_keyboard()) {
      std::cerr << "Error: Failed to initialize al_install_keyboard!" << std::endl;
      return 1;
    }
    
    //******************* CREATE AND LOAD STUFF ****************
    display = al_create_display(DISPLAY_WIDTH, DISPLAY_HEIGHT);
    if(!display) {
        std::cerr << "Error: Failed to create display!" << std::endl;
        return 1;
    }
    
    timer = al_create_timer(1.0 / FPS);
    if(!timer) {
        std::cerr << "Error: Failed to create timer!" << std::endl;
        al_destroy_display(display);
        return 1;
    }
    
    event_queue = al_create_event_queue();
    if(!event_queue) {
        fprintf(stderr, "failed to create event_queue!\n");
        al_destroy_display(display);
        al_destroy_timer(timer);
        return -1;
    }
    
    font = al_load_ttf_font((dataDir+fontName).c_str(), FONT_SIZE, 0);
    if (!font){
        std::cerr << "Error: Failed to load font!" << std::endl;
        al_destroy_display(display);
        al_destroy_timer(timer);
        al_destroy_event_queue(event_queue);
        return 1;
    }
    
    //******************* BIND AND START STUFF *****************
    al_register_event_source(event_queue, al_get_display_event_source(display));
    al_register_event_source(event_queue, al_get_timer_event_source(timer));
    al_register_event_source(event_queue, al_get_keyboard_event_source());
    
    al_start_timer(timer);
    
    return 0;
}

void UserInterface::print(){
    redraw = false;
    int line;
    const int lineStart = -2;
    const int lineDist = 2;
    al_clear_to_color(al_map_rgb(50,10,70));
    if (image){
        al_draw_scaled_bitmap(image,
                            0, 0,
                            al_get_bitmap_width(image), al_get_bitmap_height(image),
                            BORDER_SIZE, image_ypos,
                            image_width, image_height,
                            0);
    }
    line = lineStart;
    printKeybindings                        ( line += lineDist );
    printLastKeyStroke                      ( line += lineDist*2 );
    printNavigationState                    ( line += lineDist );
    printBatteryLevel                       ( line += lineDist );
    printVideoState                         ( line += lineDist );
    printContinuousImageLocalizationState   ( line += lineDist );
    printNumInliers                         ( line += lineDist );
    locator->getPoseImage(pose);
    writePoseToFile();
    al_flip_display();
}

void UserInterface::keyDownActions(ALLEGRO_EVENT &ev){
    lastKey = *al_keycode_to_name(ev.keyboard.keycode);
    manualControlState = true;
    navigator->stop();
    if (isMoveKey(ev)){
        nbrOfMoveButtonsDown++;
    }
    switch(ev.keyboard.keycode) {
        case ALLEGRO_KEY_E:
            (*this).droneController->commandCallback(DRONE_COMMAND_EMERGENCY);
            break;
        case ALLEGRO_KEY_Q:
            mainLoopRunning = false;
            (*this).droneController->commandCallback(DRONE_COMMAND_STOP);
            break;
        case ALLEGRO_KEY_ESCAPE:
            mainLoopRunning = false;
            (*this).droneController->commandCallback(DRONE_COMMAND_STOP);
            break;
        case ALLEGRO_KEY_T:
            (*this).droneController->commandCallback(DRONE_COMMAND_TAKEOFF);
            break;
        case ALLEGRO_KEY_SPACE:
            (*this).droneController->commandCallback(DRONE_COMMAND_LAND);
            break;
        case ALLEGRO_KEY_UP:
            (*this).droneController->commandCallback(DRONE_COMMAND_MOVE, 0, 0, pilotingValue);
            break;
        case ALLEGRO_KEY_DOWN:
            (*this).droneController->commandCallback(DRONE_COMMAND_MOVE, 0, 0, -pilotingValue);
            break;
        case ALLEGRO_KEY_LEFT:
            (*this).droneController->commandCallback(DRONE_COMMAND_TURN, pilotingValue);
            break;
        case ALLEGRO_KEY_RIGHT:
            (*this).droneController->commandCallback(DRONE_COMMAND_TURN, -pilotingValue);
            break;
        case ALLEGRO_KEY_W:
            (*this).droneController->commandCallback(DRONE_COMMAND_MOVE, 0, pilotingValue, 0);
            break;
        case ALLEGRO_KEY_A:
            (*this).droneController->commandCallback(DRONE_COMMAND_MOVE, -pilotingValue, 0, 0);
            break;
        case ALLEGRO_KEY_S:
            (*this).droneController->commandCallback(DRONE_COMMAND_MOVE, 0, -pilotingValue, 0);
            break;
        case ALLEGRO_KEY_D:
            (*this).droneController->commandCallback(DRONE_COMMAND_MOVE, pilotingValue, 0, 0);
            break;
        case ALLEGRO_KEY_P:
            (*this).droneController->commandCallback(DRONE_COMMAND_TAKE_AND_DOWNLOAD_IMAGE, dataDir + imageNameManual);
            break;
        case ALLEGRO_KEY_O:
            (*this).droneController->commandCallback(DRONE_COMMAND_TOGGLE_VIDEO_CAPTURE);
            break;
        case ALLEGRO_KEY_K:
            (*this).droneController->commandCallback(DRONE_COMMAND_CLEAR_MEMORY);
            break;
        case ALLEGRO_KEY_L:
            (*this).droneController->commandCallback(DRONE_COMMAND_DOWNLOAD_ALL, dataDir);
            break;
        case ALLEGRO_KEY_M:
            navigator->startMoveToWaypoint();
            manualControlState = false;
            break;
        case ALLEGRO_KEY_N:
            navigator->startSaveWaypoint();
            manualControlState = false;
            break;
        case ALLEGRO_KEY_C:
            navigator->startCalibrationDance();
            manualControlState = false;
            break;
        case ALLEGRO_KEY_H:
            locator->toggleContinuousImageLocalization();
            break;
        default:
            (*this).droneController->commandCallback(DRONE_COMMAND_STOP);
            break;
    }
}

void UserInterface::keyUpActions(ALLEGRO_EVENT &ev){
    (*this).droneController->commandCallback(DRONE_COMMAND_STOP);
    if (isMoveKey(ev)){
        nbrOfMoveButtonsDown--;
    }
}

void UserInterface::updateImage(){
    struct stat fileInfo;
    if(stat((dataDir+imageName).c_str(), &fileInfo)==0){
        time_t mod_time = fileInfo.st_mtime;
        if(mod_time > lastImageUpdateTime){
            lastImageUpdateTime = mod_time;
            pthread_mutex_lock(&mutex_global_image);
            image = al_load_bitmap((dataDir+imageName).c_str());
            pthread_mutex_unlock(&mutex_global_image);
            image_width = DISPLAY_WIDTH-BORDER_SIZE*2;
            image_height = (float)(DISPLAY_WIDTH-BORDER_SIZE*2)/al_get_bitmap_width(image)*al_get_bitmap_height(image);
            image_ypos = DISPLAY_HEIGHT-BORDER_SIZE-image_height;
        }
    }
}

bool UserInterface::isMoveKey(ALLEGRO_EVENT &ev){
    if (ev.keyboard.keycode == ALLEGRO_KEY_UP ||
        ev.keyboard.keycode == ALLEGRO_KEY_DOWN ||
        ev.keyboard.keycode == ALLEGRO_KEY_LEFT ||
        ev.keyboard.keycode == ALLEGRO_KEY_RIGHT ||
        ev.keyboard.keycode == ALLEGRO_KEY_W ||
        ev.keyboard.keycode == ALLEGRO_KEY_A ||
        ev.keyboard.keycode == ALLEGRO_KEY_S ||
        ev.keyboard.keycode == ALLEGRO_KEY_D){
        return true;
    }else{
        return false;
    }
}

void UserInterface::updateBatteryLevel(double bl){
    batteryLevel = bl;
}

void UserInterface::updatePose(Pose& newPose){
    pose.updatePose(newPose);
}

void UserInterface::updateNumInliers(int ni){
    numInliers = ni;
}

void UserInterface::updateVideoState(bool vs){
    videoState = vs;
}

void UserInterface::updateContinuousImageLocalizationState(bool cils){
    continuousImageLocalizationState = cils;
}

// void UserInterface::updateNavigationState(NAVIGATOR_SCHEMES_t ns){
void UserInterface::updateNavigationState(int ns){
    navigationState = ns;
}

void UserInterface::printKeybindings(int line){
    al_draw_multiline_textf(font, al_map_rgb(255,255,255), BORDER_SIZE, al_get_font_line_height(font)*line,
                           DISPLAY_WIDTH-BORDER_SIZE*2, al_get_font_line_height(font)*line,
                            0, "KEYBINDINGS: %s", keybindings.c_str());
}

void UserInterface::printBatteryLevel(int line){
    al_draw_textf(font, al_map_rgb(255,255,255), BORDER_SIZE, al_get_font_line_height(font)*line, 0, "BATTERY: %f", batteryLevel);
}

void UserInterface::printLastKeyStroke(int line){
    al_draw_textf(font, al_map_rgb(255,255,255), BORDER_SIZE, al_get_font_line_height(font)*line, 0, "LAST KEY: %c", lastKey);
}

void UserInterface::printIntegratedPosition(int line){
    al_draw_textf(font, al_map_rgb(255,255,255), BORDER_SIZE, al_get_font_line_height(font)*line, 0, "INTEGRATED POSITION (x|y|z): %f | %f | %f", pose.position(0), pose.position(1), pose.position(2));
}

void UserInterface::printNumInliers(int line){
    al_draw_textf(font, al_map_rgb(255,255,255), BORDER_SIZE, al_get_font_line_height(font)*line, 0, "Number of inliers: %i", numInliers);
}

void UserInterface::printVideoState(int line){
    std::string state;
    if(videoState){
        state = "on";
    }else{
        state = "off";
    }
    al_draw_textf(font, al_map_rgb(255,255,255), BORDER_SIZE, al_get_font_line_height(font)*line, 0, "Capture video: %s", state.c_str());
}

void UserInterface::printContinuousImageLocalizationState(int line){
    std::string state;
    if(continuousImageLocalizationState){
        state = "on";
    }else{
        state = "off";
    }
    al_draw_textf(font, al_map_rgb(255,255,255), BORDER_SIZE, al_get_font_line_height(font)*line, 0, "Continuous image localization: %s", state.c_str());
}

void UserInterface::printManualControlState(int line){
    std::string state;
    if(manualControlState){
        state = "on";
    }else{
        state = "off";
    }
    al_draw_textf(font, al_map_rgb(255,255,255), BORDER_SIZE, al_get_font_line_height(font)*line, 0, "Manual control: %s", state.c_str());
}

void UserInterface::printNavigationState(int line){
    std::string state;
    if (manualControlState){
        state = "manual";
    }else{
        switch (navigationState){
//             case Navigator::NAVIGATOR_SCHEME_NONE:
            case 0:
                state = "none";
                break;
//             case Navigator::NAVIGATOR_SCHEME_CALIBRATION_DANCE:
            case 1:
                state = "calibration dance";
                break;
//             case NAVIGATOR_SCHEME_MOVE_TO_WAYPOINT:
            case 2:
                state = "move to waypoint";
                break;
//             case NAVIGATOR_SCHEME_SAVE_WAYPOINT:
            case 3:
                state = "save waypoint";
                break;
            default:
                state = " ";
                break;
        }
    }
    al_draw_textf(font, al_map_rgb(255,255,255), BORDER_SIZE, al_get_font_line_height(font)*line, 0, "Navigation state: %s", state.c_str());
}

void UserInterface::writePoseToFile(){
    file.open(dataDir + filename);
//     file << pose.x << "\t" << pose.y << "\t" << pose.z << pose.angle << std::endl;
    file << pose.toString() << std::endl;
    file.close();
}