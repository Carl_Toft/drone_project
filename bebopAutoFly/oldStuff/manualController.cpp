
#include "manualController.h"
#include "global.h"
#include <iostream> // DEBUG
#include <curses.h>

ManualController::ManualController(){
    droneController = NULL;
//     pathPlanner = NULL;
    visualizer = NULL;
    locator = NULL; // DEBUG
    threadCreated = false;
    pilotingValue = 50;
    imageName = "data/manualImage.jpg";
}

 // to be run continuously in a separate thread
void ManualController::processUserInput(){
    int key = 0;    
    while(mainLoopRunning){
        
        key = getch();
        
        if(key == 'e'){
            (*this).droneController->commandCallback_manual(DRONE_COMMAND_EMERGENCY);
        }
        else if ((key == 27) || (key =='q')){
            mainLoopRunning = false;
            (*this).droneController->commandCallback_manual(DRONE_COMMAND_STOP);
        }
        else if(key == 't'){
            (*this).droneController->commandCallback_manual(DRONE_COMMAND_TAKEOFF);
        }
        else if(key == ' '){
            (*this).droneController->commandCallback_manual(DRONE_COMMAND_LAND);
        }
        else if(key == KEY_UP){
            (*this).droneController->commandCallback_manual(DRONE_COMMAND_MOVE, 0, 0, pilotingValue);
        }
        else if(key == KEY_DOWN){
            (*this).droneController->commandCallback_manual(DRONE_COMMAND_MOVE, 0, 0, -pilotingValue);
        }
        else if(key == KEY_LEFT){
            (*this).droneController->commandCallback_manual(DRONE_COMMAND_TURN, -pilotingValue);
        }
        else if(key == KEY_RIGHT){
            (*this).droneController->commandCallback_manual(DRONE_COMMAND_TURN, pilotingValue);
        }
        else if(key == 'w'){
            (*this).droneController->commandCallback_manual(DRONE_COMMAND_MOVE, pilotingValue, 0, 0);
        }
        else if(key == 'a'){
            (*this).droneController->commandCallback_manual(DRONE_COMMAND_MOVE, 0, -pilotingValue, 0);
        }
        else if(key == 's'){
            (*this).droneController->commandCallback_manual(DRONE_COMMAND_MOVE, -pilotingValue, 0, 0);
        }
        else if(key == 'd'){
            (*this).droneController->commandCallback_manual(DRONE_COMMAND_MOVE, 0, pilotingValue, 0);
        }
        else if(key == 'p'){
            (*this).droneController->commandCallback_manual(DRONE_COMMAND_TAKE_IMAGE);
        }
        else if(key == 'l'){
            (*this).droneController->commandCallback_manual(DRONE_COMMAND_DOWNLOAD_IMAGE, imageName);
        }
        else if(key == 'o'){
            (*this).droneController->commandCallback_manual(DRONE_COMMAND_CLEAR_MEMORY);
        }
        else if(key == 'k'){
            (*this).locator->updatePoseImageManual(); // DEBUG
        }
        else{
            (*this).droneController->commandCallback_manual(DRONE_COMMAND_STOP);
        }
        
        if(key == ERR){
            key = ' ';
        }
        visualizer->updateLastKey(key);
        
        usleep(10);
    }
    
    pthread_exit(NULL);
}

// helper function to make a thread use a member-function
// void ManualController::*processUserInput_helper(void *context){
//     ((ManualController *) context)->processUserInput();
//     return NULL;
// }

void ManualController::setDroneController(DroneController * d){
    (*this).droneController = d;
}

// void ManualController::setPathPlanner(PathPlanner * p){
//     pathPlanner = p;
// }

void ManualController::setVisualizer(Visualizer * v){
    visualizer = v;
}

void ManualController::setLocator(Locator * l){ // DEBUG
    locator = l;
}

int ManualController::setup(){
    if( (*this).droneController == NULL || visualizer == NULL ){
        return 1;
    }
    initscr();
    cbreak(); // Line buffering disabled
    keypad(stdscr, TRUE);
    noecho(); // Don't echo() while we do getch
    timeout(100);
    refresh();
    pthread_create(&thread, NULL, &processUserInput_helper, (void *)this);
    threadCreated = true;
    return 0;
}

void ManualController::cleanup(){
    mainLoopRunning = false;
    if (threadCreated){
        pthread_join(thread, NULL);
    }
    endwin();
}


void ManualController::setManualControlFlag(bool flag){
    (*this).droneController->setManualControlFlag(flag);
}

bool ManualController::getManualControlFlag(){
    (*this).droneController->getManualControlFlag();
}
