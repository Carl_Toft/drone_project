%% Convert image to PGM
filename_in = '../data4/image055.jpg';
filename_out = '../data4/image055.pgm';

a = imread(filename_in);
imwrite(a, filename_out, 'pgm', 'Encoding', 'rawbits', 'MaxValue', 255);

% then copy the pgm-file to the right dir

%% Format the point cloud data and save in tsv

% load the SfM-data first

formatPointCloud(u, U, true);

% then copy the tsv-files to the right dir

%% Save Carls version in tsv

clear
load sift_test.mat

dlmwrite('descCloud.tsv', SIFT_vectors_3D, '\t');
dlmwrite('coordCloud.tsv', U, '\t');

% then copy the tsv-files to the right dir

%% Plot the results

% load the SfM-data first
clear
% load ../data4/str_mot2.mat

dataDir = '../data5/';
p = importdata(strcat(dataDir, 'cameraMatrix.tsv'));
U = importdata(strcat(dataDir, 'coordCloud.tsv'));

x = - p(1:3, 1:3)' * p(1:3, 4);
y = 

close all
% plot(motion(P), structure(U))
plot(structure(U))
hold on
plot(motion(p), 'r')
plot3(x(1), x(2), x(3), 'g*', 'MarkerSize', 10)
axisLimit = 0.08;
axis([-axisLimit axisLimit -axisLimit axisLimit -axisLimit axisLimit -axisLimit axisLimit])