function [U, descriptors] = formatPointCloud(u, U, average)
% Format a point cloud of 3D-point/descriptor pairs

% Input:
%   u       - struct with the image data from the SfM program
%   U       - 4xn matrix containing the 3D positions
%   average - true = average, false = first occurance
% Output:
%   coordCloud.tsv  - 4xn matrix of 3D coordinates (= U)
%   descCloud.tsv   - 128xn matrix of the corresponding descriptors

% The script gathers all the corresponding descriptors to
% the 3D-points and averages the descriptors when there are
% more than one present. It could also just take the first
% occurance instead of the average.

% *** Initialize the storage ***
descriptors = zeros(128, size(U,2));
nbrOfKeyPoints = zeros(1, size(U,2));

% *** Gather the data ***
for i_image = 1 : size(u.index, 1)
    for i_index = 1 : size(u.index{i_image}, 2)
        index = u.index{i_image}(i_index);
        if average
            descriptors(:, index) = descriptors(:, index) + double(u.sift{i_image}(:, i_index));
            nbrOfKeyPoints(index) = nbrOfKeyPoints(index) + 1;
        else
            if nbrOfKeyPoints(index) == 0
                descriptors(:, index) = double(u.sift{i_image}(:, i_index));
                nbrOfKeyPoints(index) = nbrOfKeyPoints(index) + 1;
            end
        end
    end
end
for i = 1 : size(descriptors, 2)
    descriptors(:, i) = descriptors(:, i)./nbrOfKeyPoints(i);
end

% *** Write to files ***
dlmwrite('coordCloud.tsv', U, '\t');
dlmwrite('descCloud.tsv', descriptors, '\t');