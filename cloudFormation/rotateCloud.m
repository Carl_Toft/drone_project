clear

z = [0,0,0]';
a = [1,1,1]';
b = [0,1,1]';

%%
u  = cross(a,b); u = u/norm(u);
v = acos(dot(a,b)/norm(a)/norm(b));
crossMatrix = [0,-u(3),u(2);u(3),0,-u(1);-u(2),u(1),0];
R = eye(3)*cos(v) + crossMatrix*sin(v) + (1-cos(v))*u*u'; % rotation matrix
s = norm(b)/norm(a); % scale factor
c = s*R*a;
%%
M = getTransformationMatrix(a,b);
M = M(1:3,1:3);
c = M*a;

%%
clf
hold on
quiver3(z(1), z(2), z(2), a(1), a(2), a(3),'b')
quiver3(z(1), z(2), z(2), b(1), b(2), b(3),'g')
% quiver3(z(1), z(2), z(2), u(1), u(2), u(3),'r')
quiver3(z(1), z(2), z(2), c(1), c(2), c(3),'k')
axis equal

%%
clear
scale = 100;
C = load('coordCloud.tsv');
a = [-0.2,-1,-0.25,1]';
b = [0,0,1,1]'*scale;
M = getTransformationMatrix(a,b);
C = M*C;

%%
x = [0,0,0;1,0,0]';
y = [0,0,0;0,1,0]';
z = [0,0,0;0,0,1]';
t = [0,0,0;a(1:3)']';
clf
hold on
plot3(C(1,:),C(2,:),C(3,:),'*')
plot3(x(1,:),x(2,:),x(3,:),'r');
plot3(y(1,:),y(2,:),y(3,:),'g');
plot3(z(1,:),z(2,:),z(3,:),'b');
% plot3(t(1,:),t(2,:),t(3,:),'k');
axisLimit = 0.08 * scale;
axis([-axisLimit axisLimit -axisLimit axisLimit -axisLimit axisLimit -axisLimit axisLimit])