#ifndef _TSV_TO_MATRIX_H
#define _TSV_TO_MATRIX_H

#include <Eigen/Dense>
#include <string>

int tsvToMatrix (std::string fileName, Eigen::MatrixXd& matrix, const int nRows);

int matrixToTsv (std::string fileName, Eigen::MatrixXd& matrix);

#endif