#ifndef _GET_MATCHES_H
#define _GET_MATCHES_H

#include <Eigen/Dense>
#include <stdint.h>
#include <string>

extern "C"{
    #include <vl/kdtree.h>
}

int getSiftPoints (std::string imageName, Eigen::MatrixXd& coordinates, Eigen::MatrixXd& descriptors);

int tsvToMatrix (std::string fileName, Eigen::MatrixXd& matrix, const int nRows);

int matrixToTsv (std::string fileName, Eigen::MatrixXd& matrix);

void initializeKDTree (VlKDForest*& kdtree, Eigen::MatrixXd& data);

void matchSiftPoints (VlKDForest*& kdtree, Eigen::MatrixXd& descImage, Eigen::MatrixXd& descCloud, Eigen::MatrixXd& coordImage_in, Eigen::MatrixXd& coordCloud_in, Eigen::MatrixXd& coordImage_out, Eigen::MatrixXd& coordCloud_out);

void normColumns(Eigen::MatrixXd& matrix);

#endif