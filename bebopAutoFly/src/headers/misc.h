#ifndef _MISC
#define _MISC

#include <Eigen/Eigen>
#include <string>

class Pose{
    public:
        // Position
        Eigen::Vector3d position;
        // Principal axis
        Eigen::Vector3d heading;
        // Camera matrix
        Eigen::Matrix<double, 3, 4> cameraMatrix;
        
        Pose();
        Pose(Eigen::Vector3d posIn, Eigen::Vector3d headIn);
        Pose(double ix, double iy, double iz, double ipax, double ipay, double ipaz);
//         ~Pose();
        
        void updatePose(Pose &newPose);
        void updateWithCameraPose(Eigen::MatrixXd & cameraPose);
        
        double distance(Pose &otherPose);
        std::string toString();
};

#endif