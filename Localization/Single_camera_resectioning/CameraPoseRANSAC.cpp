#include "p3p.h"
#include <Eigen/Eigen>
#include <cassert>

Eigen::MatrixXd transformToCameraFromP3PSol(const Eigen::MatrixXd& sol)
{
    Eigen::MatrixXd P(3,4);
    Eigen::MatrixXd R(3,3);
    R << sol.col(0), sol.col(1), sol.col(2);
    R.transposeInPlace();
    P << R, (-R*sol.col(3));

    return P;
}

// Finds the number of inliers when points are projected using camera matrix P. impoints are image points as unit 3x1 vectors
// and spacePoints are the 3D points represented as homogenous 4x1 vectors. Tolerance is the maximul allowed angle deviation 
// for an inlier 
unsigned int findNumInliers(const Eigen::MatrixXd& P, const Eigen::MatrixXd& impoints, const Eigen::MatrixXd& spacePoints, double tol)
{
    double cosEps = cos(tol);
    unsigned int inliers = 0;

    auto projectedPoints = P*spacePoints;
    auto values = projectedPoints.transpose()*impoints;
    auto dotProd = values.diagonal();
    auto xNorms = projectedPoints.colwise().norm().transpose();
    inliers = (dotProd.array() > xNorms.array()*cosEps).count();

    return inliers;
}

// Finds camera pose using RANSAC. Inputs are two 3xN matrices for image and spacepoints. Image points are represented as unit vectors. 
// Also, number of RANSAC iterations and tolerance are supplied. The output is the best camera position and rotation found.  
Eigen::MatrixXd cameraPoseRANSAC(const Eigen::MatrixXd& impoints, const Eigen::MatrixXd& spacePoints, unsigned int numIterations, double tol, unsigned int& oNumInliers)
{
    assert(impoints.cols() == spacePoints.cols());
    auto N = spacePoints.cols();

    // Create homogenous version of spacePoints
    Eigen::MatrixXd spacePointsHomog(4,N);
    Eigen::MatrixXd ones(1,N);
    ones.setOnes();
    spacePointsHomog << spacePoints, ones;


    Eigen::MatrixXd X(3,3), x(3,3), P, bestP;
    unsigned int maxInliers = 0, inliers;

    for (int iter = 0; iter < numIterations; iter++) {
        // Randomly select three distinct correspondences
        int n1 = rand() % N;

        int n2 = rand() % N;
        while (n2 == n1) {
            n2 = rand() % N;
        }

        int n3 = rand() % N;
        while (n3 == n1 || n3 == n2) {
            n3 = rand() % N;
        }

        X << spacePoints.col(n1), spacePoints.col(n2), spacePoints.col(n3);
        x << impoints.col(n1), impoints.col(n2), impoints.col(n3);

        // Solve P3P problem using these correspondences
        Eigen::Matrix<Eigen::Matrix<double,3,4>,4,1> solutions;
        monocular_pose_estimator::P3P::computePoses(x,X,solutions);

        // For each computed pose, calculate number of inliers, i.e.,
        // points whose reprojection are within the specified tolerance
        for (int cam = 0; cam < 4; cam++) {
            P = transformToCameraFromP3PSol(solutions(cam,0));
            inliers = findNumInliers(P,impoints,spacePointsHomog,tol);
            if (inliers > maxInliers) {
                maxInliers = inliers;
                bestP = P;
            }
        }
    }

    oNumInliers = maxInliers;

    return bestP;
}