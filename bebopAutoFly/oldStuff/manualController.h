#ifndef _MANUAL_CONTROLLER
#define _MANUAL_CONTROLLER

#include "droneController.h"
// #include "navigator.h"
#include "visualizer.h"
#include <pthread.h>

// Forward declarations
class DroneController;
// class PathPlanner;
class Visualizer;
class Locator; // DEBUG

class ManualController{
    private:
        DroneController * droneController;
//         PathPlanner * pathPlanner;
        Visualizer * visualizer;
        Locator * locator; // DEBUG
        
        double pilotingValue;
        std::string imageName;
        
        pthread_t thread;
        bool threadCreated;
        
        void setManualControlFlag(bool flag);
        bool getManualControlFlag();
        
        void processUserInput(); // to be run continuously in a separate thread
        
        static void *processUserInput_helper(void *context){ // helper function to make a thread use a member-function
            ((ManualController *) context)->processUserInput();
            return NULL;
        }
    public:
        ManualController();
        //~ManualController();
        
        void setDroneController(DroneController * d);
//         void setPathPlanner(PathPlanner * p);
        void setVisualizer(Visualizer * v);
        void setLocator(Locator * l); // DEBUG
        
        int setup();
        void cleanup();
};

#endif