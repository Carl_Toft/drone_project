#include "p3p.h" 
#include <iostream>
#include <Eigen/Eigen>
#include <unsupported/Eigen/MatrixFunctions>  // for matrix exponential

Eigen::MatrixXd cameraPoseRANSAC(const Eigen::MatrixXd& impoints, const Eigen::MatrixXd& spacePoints, unsigned int numIterations, double tol);
Eigen::MatrixXd PerformLevMarqStep(Eigen::MatrixXd& P, Eigen::MatrixXd& X, Eigen::MatrixXd x, double lambda);

Eigen::MatrixXd read3LineMatrixFromStdin()
{
	std::vector<std::string> text;
	std::string line;
	for (int i = 0; i < 3; i++) {
		if (std::getline(std::cin, line))
			text.push_back(line);
	}
	
	// assert(text.size() == 3); 
	
	std::vector<double> numbers;
	double number;
	std::stringstream ss(text[0]);
	while (ss >> number)
		numbers.push_back(number); 
	int N = numbers.size(); 
	Eigen::MatrixXd readMatrix(3,N);
	for (int i = 0; i < N; i++)
		readMatrix(0,i) = numbers[i];
	for (int i = 1; i < 3; i++) {
		int count = 0; 
		std::stringstream ss(text[i]);
		while (ss >> number) {
			readMatrix(i,count) = number;
			count++;
		}
		// assert(count == N); 
	}
	
	return readMatrix; 
}

int main()
{	
	srand(time(NULL)); 
	
	auto X = read3LineMatrixFromStdin(); 
	auto x = read3LineMatrixFromStdin(); 
	auto P = cameraPoseRANSAC(x,X,100,0.002); 

	std::cout << P << "\n\n";

    // Find homogenous image coordinates by normalizing the last column
    Eigen::MatrixXd xHomogen(3,x.cols());
    for (int i = 0; i < x.cols(); i++)
        xHomogen.col(i) = x.col(i)/x(2,i);

    // Apply slight perturbation to the rotation and test Levenberg-Marquardt
    Eigen::Matrix3d A(3,3);
    double a1 = 0.01, a2 = -0.02, a3 = 0.0015;
    A << 0.0, -a1, -a2, a1, 0.0, -a3, a2, a3, 0.0;
    Eigen::Matrix3d deltaRot = A.exp();
    Eigen::Matrix3d newR = deltaRot*P.block(0,0,3,3);
    Eigen::MatrixXd P_perturb(3,4);
    P_perturb << newR, newR*P.block(0,3,3,1);
    std::cout << "Perturbed camera matrix:\n" << P_perturb << "\n\n";

    // Perform a step of Levenberg-Marquardt
    for (int i = 0; i < 5; i++) {
        P_perturb = PerformLevMarqStep(P_perturb, X, xHomogen, 0.005);
        std::cout << "Iteration " << i << ": \n" << P_perturb << "\n";
    }
	return 0;
}
