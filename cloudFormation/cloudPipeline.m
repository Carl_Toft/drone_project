function cloudPipeline(dirName)
    curDir = pwd;
    resultsDir = 'results';

    cd(dirName);
    settings = reconstr_setup;
    load 'red_str_mot.mat'
    cd(curDir);
    
    formatPointCloud(u, U, true);
    tsv2pcd('coordCloud');
    
    formatPointCloudColor(u, U, false, settings);
    
    load(strcat(dirName,'/Calib_Results.mat'));
    dlmwrite('calibrationMatrix.tsv', KK, '\t');
    
    mkdir(resultsDir);
    movefile('calibrationMatrix.tsv',   strcat(resultsDir, '/calibrationMatrix.tsv'));
    movefile('coordCloud.tsv',          strcat(resultsDir, '/coordCloud.tsv'));
    movefile('coordCloud.pcd',          strcat(resultsDir, '/coordCloud.pcd'));
    movefile('descCloud.tsv',           strcat(resultsDir, '/descCloud.tsv'));
    movefile('colorCloud.tsv',           strcat(resultsDir, '/colorCloud.tsv'));
end