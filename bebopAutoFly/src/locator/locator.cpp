#include "locator.h"
#include "getMatches.h"
#include "cameraPose.h"
#include "tsvToMatrix.h"
#include <Eigen/Dense>
// #include <iostream>
#include <string>
#include <stdlib.h>
#include <iostream>

extern "C"{
    #include <vl/kdtree.h>
}

Locator::Locator(){
    userInterface = NULL;
    droneController = NULL;
    mutex_pose = PTHREAD_MUTEX_INITIALIZER;
    mutex_poseIMU = PTHREAD_MUTEX_INITIALIZER;
    mutex_poseImage = PTHREAD_MUTEX_INITIALIZER;
    threadCreated = false;
    taskToStart = LOCATOR_TASK_NONE;
    continuousImageLocalizationRunning = false;
    dataDir = "data/";
//     imageNameJPG = "testImage.jpg";
//     imageNamePGM = "testImage.pgm";
    imageNameJPG = "lastImage.jpg";
    imageNamePGM = "lastImage.pgm";
    inlierThreshold = 20; // TODO experiment with this value
    scale = 1e6;
    scaleUpdates = 0;
}

int Locator::setup(){
    // *************** Check bindings to other objects ****************************
    if(userInterface == NULL || droneController == NULL){
        mainLoopRunning = false;
        return 1;
    }
    
    // *************** Setup the image processing *********************************
    
    int err; // error code
    
    // Read data (cloud data and calibration matrix)
    err = 0;
    err += tsvToMatrix (dataDir + "coordCloud.tsv", coordCloud_in, 4);
    err += tsvToMatrix (dataDir + "descCloud.tsv", descCloud, 128);
    err += tsvToMatrix (dataDir + "calibrationMatrix.tsv", calib, 3);
    
    if(err){
        std::cerr << "Failed to read one or more cloud data files. Terminating program." << std::endl;
        mainLoopRunning = false;
        return 1;
    }
    
    normColumns(descCloud);
    
    // Construct KDTree from cloud data
    initializeKDTree (kdtree, descCloud);
    
    return 0;
}

void Locator::close(){
    mainLoopRunning = false;
    if (threadCreated){
        pthread_join(thread, NULL);
    }    
    pthread_mutex_destroy(&mutex_pose);
    pthread_mutex_destroy(&mutex_poseIMU);
    pthread_mutex_destroy(&mutex_poseImage);
    vl_kdforest_delete(kdtree);
}

void Locator::setUserInterface(UserInterface * u){
    userInterface = u;
}

void Locator::setDroneController(DroneController * d){
    (*this).droneController = d;
}

void Locator::setScale(double s){
    scale = (scale * scaleUpdates + s) / (scaleUpdates+1);
    scaleUpdates++;
}

double Locator::getScale(){
    return scale;
}

void Locator::getPose(Pose &poseToUpdate){
    // Right now this function is the only one that uses more than
    // one mutex at the same time. If another function would do the
    // same there would be a risk of the dining philosophers problem,
    // but with only one such function it should be ok.
    pthread_mutex_lock (&mutex_pose);
        pthread_mutex_lock (&mutex_poseIMU);
            pthread_mutex_lock (&mutex_poseImage);
                pose.position = poseImage.position + poseIMU.position - poseIMUByLastImagePose.position;
                pose.heading = poseImage.heading; // TODO until IMU heading is working
            pthread_mutex_unlock (&mutex_poseImage);
//             pose.heading = poseIMU.heading; // TODO use this when IMU heading is done
        pthread_mutex_unlock (&mutex_poseIMU);
        poseToUpdate.updatePose (pose);
    pthread_mutex_unlock (&mutex_pose);
}

void Locator::getPoseIMU(Pose &poseToUpdate){
    pthread_mutex_lock (&mutex_poseIMU);
    poseToUpdate.updatePose (poseIMU);
    pthread_mutex_unlock (&mutex_poseIMU);
}

void Locator::getPoseImage(Pose &poseToUpdate){
    pthread_mutex_lock (&mutex_poseImage);
    poseToUpdate.updatePose (poseImage);
    pthread_mutex_unlock (&mutex_poseImage);
}

bool Locator::getContinuousImageLocalizationRunning(){
    return continuousImageLocalizationRunning;
}

int Locator::startOneShotImageLocalization(Pose& poseToUpdate){
    if (threadCreated || (*this).droneController==NULL || userInterface==NULL){
        return 1;
    }else{
        taskToStart = LOCATOR_TASK_ONE_SHOT_IMAGE_LOCALIZATION;
        pthread_create(&thread, NULL, &thread_starter, (void *)this);
        threadCreated = true;
        pthread_join(thread, NULL);
        getPoseImage(poseToUpdate);
        return 0;
    }
}

int Locator::startContinuousImageLocalization(){
    if (threadCreated || (*this).droneController==NULL || userInterface==NULL){
        return 1;
    }else{
        taskToStart = LOCATOR_TASK_CONTINUOUS_IMAGE_LOCALIZATION;
        continuousImageLocalizationRunning = true;
        pthread_create(&thread, NULL, &thread_starter, (void *)this);
        threadCreated = true;
        return 0;
    }
}

void Locator::stopContinuousImageLocalization(){
    continuousImageLocalizationRunning = false;
}

void Locator::stopContinuousImageLocalization_blocking(){
    stopContinuousImageLocalization();
    if (threadCreated || taskToStart == LOCATOR_TASK_CONTINUOUS_IMAGE_LOCALIZATION){
        pthread_join(thread, NULL);
    }
}

void Locator::toggleContinuousImageLocalization(){
    if (continuousImageLocalizationRunning){
        stopContinuousImageLocalization();
    }else{
        startContinuousImageLocalization();
    }
}

void *Locator::thread_starter(void *context){ // helper function to make a thread use a member-function
    Locator * loc = (Locator *) context;
    switch( (*loc).taskToStart){
        case LOCATOR_TASK_ONE_SHOT_IMAGE_LOCALIZATION:
            loc->oneShotImageLocalization();
            break;
        case LOCATOR_TASK_CONTINUOUS_IMAGE_LOCALIZATION:
            loc->continuousImageLocalization();
            break;
        case LOCATOR_TASK_NONE:
            break;
        default:
            break;
    }
    (*loc).taskToStart = LOCATOR_TASK_NONE;
    (*loc).threadCreated = false;
    pthread_exit(NULL);
}

int Locator::takeNewImage(){
    return (*this).droneController->commandCallback(DRONE_COMMAND_TAKE_AND_DOWNLOAD_IMAGE, dataDir+imageNameJPG);
}

void Locator::oneShotImageLocalization(){
    if(!takeNewImage()){
//         pthread_mutex_lock(&mutex_poseIMU);
//         poseIMUByImageUnderProcessing.updatePose(poseIMU);
//         pthread_mutex_unlock(&mutex_poseIMU);
        if(!updatePoseImage()){
//             pthread_mutex_lock(&mutex_pose);
//             poseIMUByLastImagePose.updatePose(poseIMUByImageUnderProcessing);
//             pthread_mutex_unlock(&mutex_pose);
        }
    }
}

void Locator::continuousImageLocalization(){
    userInterface->updateContinuousImageLocalizationState(continuousImageLocalizationRunning);
    while(mainLoopRunning && continuousImageLocalizationRunning){
        oneShotImageLocalization();
        usleep(10);
    }
    continuousImageLocalizationRunning = false;
    userInterface->updateContinuousImageLocalizationState(continuousImageLocalizationRunning);
}

void Locator::updateCloud(Eigen::Matrix<double, 4, 4> transformationMatrix){
    // TODO mutex
    coordCloud_in = transformationMatrix * coordCloud_in;
}

void Locator::updatePoseIMUPosition(double speedX, double speedY, double speedZ, double dTime){
    pthread_mutex_lock (&mutex_poseIMU);
    poseIMU.position(0) += speedX * dTime;
    poseIMU.position(1) += speedY * dTime;
    poseIMU.position(2) += speedZ * dTime;
    pthread_mutex_unlock (&mutex_poseIMU);
}

void Locator::updatePoseIMUHeading(double roll, double pitch, double yaw){
    // TODO should convert from rpy-coords to unit-vector-coords!
    return; // DEBUG deactivating this function temporarily...
    pthread_mutex_lock (&mutex_poseIMU);
    poseIMU.heading(0) = roll;
    poseIMU.heading(1) = pitch;
    poseIMU.heading(2) = yaw;
    pthread_mutex_unlock (&mutex_poseIMU);
}

int Locator::updatePoseImage(){
    Eigen::MatrixXd coordImage_in;
    Eigen::MatrixXd coordImage_out;
    Eigen::MatrixXd descImage;
    Eigen::MatrixXd coordCloud_out;
    unsigned int numInliers;
    
    // Convert image to gray scale
    std::string convertCall = "magick convert " + dataDir + imageNameJPG + " -colorspace gray -depth 8 " + dataDir + imageNamePGM;
    system(convertCall.c_str());
    
    // Get SIFT-points from the current image
    if( getSiftPoints (dataDir + imageNamePGM, coordImage_in, descImage) ){
        std::cerr << "Failed to extract SIFT-points." << std::endl;
        return 1;
    }
    
    normColumns(descImage);
    
    // Match the image points to the cloud points
    matchSiftPoints (kdtree, descImage, descCloud, coordImage_in, coordCloud_in, coordImage_out, coordCloud_out);
    
    if( coordImage_out.cols() < 3 ){
        userInterface->updateNumInliers(0);
        return 1;
    }
    
    // Fix the coordinate matrices
        // Apply inverse camera matrix to the image points
    coordImage_out = calib.inverse() * coordImage_out;
        // Norm the columns of the image point matrix
    normColumns(coordImage_out);
        // Remove the forth row of the space matrix
    coordCloud_out.conservativeResize(3,coordCloud_out.cols());
    
    // Compute the camera pose from the image-cloud coordinate correlations
    numInliers = 0;
    Eigen::MatrixXd cameraPose = cameraPoseRANSAC(coordImage_out, coordCloud_out, 100, 0.002, numInliers); // params: 100, 0.002
    userInterface->updateNumInliers(numInliers);
    
    /*
    std::cout << "number of keypoints: " << coordImage_in.cols() << std::endl; // DEBUG
    std::cout << "number of matches: " << coordImage_out.cols() << std::endl; // DEBUG
    std::cout << "number of inliers: " << numInliers << std::endl; // DEBUG
    std::cout << "********************************" << std::endl; // DEBUG
    */
    
    // Save the result
    if (numInliers > inlierThreshold){
        pthread_mutex_lock (&mutex_poseImage);
        poseImage.updateWithCameraPose(cameraPose);
//         userInterface->updatePose(poseImage);
        pthread_mutex_unlock (&mutex_poseImage);
        return 0;
    }
    
    return 1;
}