#ifndef _USERINTERFACE
#define _USERINTERFACE

#include "navigator.h"
#include "droneController.h"
#include "locator.h"
#include "misc.h"
#include <pthread.h>
#include <string>
#include <fstream>
#include <allegro5/allegro.h>
#include <allegro5/allegro_image.h>
#include <allegro5/allegro_font.h>

// Forward declarations
class DroneController;
class Navigator;
class Locator;
// class PathPlanner;

class UserInterface{
    private:
        DroneController * droneController;
        Navigator * navigator;
        Locator * locator;
//         PathPlanner * pathPlanner;
        
        std::string keybindings;
        char lastKey;
        double batteryLevel; // percent
        int numInliers;
        bool videoState;
        Pose pose;
        bool continuousImageLocalizationState;
//         NAVIGATOR_SCHEMES_t navigationState;
        int navigationState;
        
        bool manualControlState;
        double pilotingValue;
        int nbrOfMoveButtonsDown;
        
        std::ofstream file;
        std::string dataDir;
        std::string filename;
        std::string imageName;
        std::string imageNameManual;
        std::string fontName;
        
        pthread_t thread;
        bool threadCreated;
        time_t lastImageUpdateTime;
        
        ALLEGRO_DISPLAY *display;
        ALLEGRO_BITMAP  *image;
        ALLEGRO_FONT *font;
        ALLEGRO_EVENT_QUEUE *event_queue;
        ALLEGRO_TIMER *timer;
        bool redraw;
        
        int DISPLAY_WIDTH;
        int DISPLAY_HEIGHT;
        int FONT_SIZE;
        int BORDER_SIZE;
        float FPS;
        float image_width;
        float image_height;
        float image_ypos;
        
        void run(); // to be run continuously in a separate thread
        
        static void *run_helper(void *context); // helper function to make a thread use a member-function
        
        int setup();
        void print();
        void keyDownActions(ALLEGRO_EVENT &ev);
        void keyUpActions(ALLEGRO_EVENT &ev);
        bool isMoveKey(ALLEGRO_EVENT &ev);
        
        void updateImage();
        
        void printKeybindings(int line);
        void printBatteryLevel(int line);
        void printIntegratedPosition(int line);
        void printLastKeyStroke(int line);
        void printNumInliers(int line);
        void printVideoState(int line);
        void printContinuousImageLocalizationState(int line);
        void printManualControlState(int line);
        void printNavigationState(int line);
        
        void writePoseToFile();
        
    public:
        UserInterface();
        //~UserInterface();
        
        void setDroneController(DroneController * d);
        void setNavigator(Navigator * n);
        void setLocator(Locator * l);
//         void setPathPlanner(PathPlanner * p);
        
        bool getManualControlState();
        
        int start();
        void close();
        
        void updateBatteryLevel(double bl);
        void updateLastKey(char k);
        void updateNumInliers(int ni);
        void updateVideoState(bool vs);
        void updatePose(Pose &newPose);
        void updateContinuousImageLocalizationState(bool cils);
//         void updateNavigationState(NAVIGATOR_SCHEMES_t ns);
        void updateNavigationState(int ns);
};

#endif