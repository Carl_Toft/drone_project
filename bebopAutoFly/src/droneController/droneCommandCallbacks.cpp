#include "droneController.h"

int DroneController::commandCallback(DRONE_COMMANDS_t command){
    switch (command){
        case DRONE_COMMAND_EMERGENCY:
            emergency();
            break;
        case DRONE_COMMAND_STOP:
            stop();
            break;
        case DRONE_COMMAND_TAKEOFF:
            takeOff();
            break;
        case DRONE_COMMAND_LAND:
            land();
            break;
        case DRONE_COMMAND_TAKE_IMAGE:
            return takeImage();
            break;
        case DRONE_COMMAND_TOGGLE_VIDEO_CAPTURE:
            toggleVideoCapture();
            break;
        case DRONE_COMMAND_CLEAR_MEMORY:
            clearMemory();
            break;
        case DRONE_COMMAND_DO_NOTHING:
            stop();
            break;
        default:
            stop();
            break;
    }
    return 0;
}

int DroneController::commandCallback(DRONE_COMMANDS_t command, double angle){
    switch (command){
        case DRONE_COMMAND_TURN:
            turn(angle);
            break;
        case DRONE_COMMAND_TURN_BY:
            turnBy(angle);
            break;
        default:
            stop();
            break;
    }
    return 0;
}

int DroneController::commandCallback(DRONE_COMMANDS_t command, std::string dest){
    switch (command){
        case DRONE_COMMAND_DOWNLOAD_IMAGE:
            downloadImage(dest);
            break;
        case DRONE_COMMAND_TAKE_AND_DOWNLOAD_IMAGE:
            return takeAndDownloadImage(dest);
            break;
        case DRONE_COMMAND_DOWNLOAD_ALL:
            downloadAll(dest);
            break;
        default:
            stop();
            break;
    }
    return 0;
}

int DroneController::commandCallback(DRONE_COMMANDS_t command, double x, double y, double z){
    switch (command){
        case DRONE_COMMAND_MOVE:
            if(x == 0 && y == 0){
                gaz(z);
            }else if(x == 0 && z == 0){
                pitch(y);
            }else if(y == 0 && z == 0){
                roll(x);
            }
            //move(x,y,z); // The move(x,y,z) function does not work...
            break;
        case DRONE_COMMAND_MOVE_BY:
            moveBy(x, y, z);
            break;
        default:
            stop();
            break;
    }
    return 0;
}