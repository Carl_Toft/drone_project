#include "droneController.h"
#include "global.h"
#include "FTPClient.h"
#include <string>
#include <cstdio>
#include <iostream>

extern "C"{
    #include <libARSAL/ARSAL.h>
    #include <libARController/ARController.h>
    #include <libARDiscovery/ARDiscovery.h>
}

bool DroneController::firstTimer = false;
time_t DroneController::timer;

DroneController::DroneController(){
    userInterface = NULL;
    locator = NULL;
    navigator = NULL;
    deviceController = NULL;
    videoCaptureOn = false;
}

void DroneController::setLocator(Locator * l){
    locator = l;
}

void DroneController::setNavigator(Navigator * n){
    navigator = n;
}

void DroneController::setUserInterface(UserInterface * u){
    userInterface = u;
}

int DroneController::start(){
    if(userInterface == NULL || locator==NULL || navigator==NULL){
        mainLoopRunning = false;
        return 1;
    }
    
    // init stuff
    sem_init(&sem_droneControllerState, 0, 0);
    sem_init(&sem_takeImage, 0, 0);
    sem_init(&sem_activateVideoCapture, 0, 0);
    mutex_takeImage = PTHREAD_MUTEX_INITIALIZER;
    firstTimer = true;
    
    ARDISCOVERY_Device_t *device = NULL;
    eARCONTROLLER_ERROR error = ARCONTROLLER_OK;
    eARDISCOVERY_ERROR errorDiscovery = ARDISCOVERY_OK;
    
    // setup discovery device
    device = ARDISCOVERY_Device_New (&errorDiscovery);
    errorDiscovery = ARDISCOVERY_Device_InitWifi (device, ARDISCOVERY_PRODUCT_BEBOP_2, "bebop2", "192.168.42.1", 44444);
    
    if (errorDiscovery != ARDISCOVERY_OK){
        ARDISCOVERY_Device_Delete (&device);
        mainLoopRunning = false;
        return 1;
    }
    
    // setup device controller
    deviceController = ARCONTROLLER_Device_New (device, &error);
    ARDISCOVERY_Device_Delete (&device);
    
    if (error != ARCONTROLLER_OK){
        mainLoopRunning = false;
        return 1;
    }
    
    // add the state change callback to be informed when the device controller starts, stops...
    error = ARCONTROLLER_Device_AddStateChangedCallback (deviceController, DroneController::controllerStateCallback, deviceController);
    
    // add the command received callback to be informed when a command has been received from the device
    error = ARCONTROLLER_Device_AddCommandReceivedCallback (deviceController, DroneController::droneEventCallback, deviceController);
    
    // connect
    error = ARCONTROLLER_Device_Start (deviceController);
    
    if (error != ARCONTROLLER_OK){
        mainLoopRunning = false;
        return 1;
    }
    
    sem_wait(&sem_droneControllerState);
    
    // Send some configuration commands to the drone
    deviceController->aRDrone3->sendPictureSettingsVideoStabilizationMode(deviceController->aRDrone3, ARCOMMANDS_ARDRONE3_PICTURESETTINGS_VIDEOSTABILIZATIONMODE_MODE_NONE);
    deviceController->aRDrone3->sendPictureSettingsVideoAutorecordSelection(deviceController->aRDrone3, 0, 0);
    deviceController->aRDrone3->sendPictureSettingsPictureFormatSelection(deviceController->aRDrone3, ARCOMMANDS_ARDRONE3_PICTURESETTINGS_PICTUREFORMATSELECTION_TYPE_SNAPSHOT);
//     deviceController->aRDrone3->sendMediaStreamingVideoEnable(deviceController->aRDrone3, 0);
    
    // Clear drones memory
    clearMemory();
    
    return 0;
}

void DroneController::close(){
    mainLoopRunning = false;
    
    eARCONTROLLER_ERROR error;
    eARCONTROLLER_DEVICE_STATE deviceState;
    
    if(deviceController != NULL){
        deviceState = ARCONTROLLER_Device_GetState (deviceController, &error);
        if ((error == ARCONTROLLER_OK) && (deviceState != ARCONTROLLER_DEVICE_STATE_STOPPED)){
            error = ARCONTROLLER_Device_Stop (deviceController);
            if (error == ARCONTROLLER_OK){
                // wait for state update
                sem_wait(&sem_droneControllerState);
            }
        }
        ARCONTROLLER_Device_Delete (&deviceController);
    }
    
    sem_destroy(&sem_droneControllerState);
    sem_destroy(&sem_takeImage);
    sem_destroy(&sem_activateVideoCapture);
    pthread_mutex_destroy(&mutex_takeImage);
}

void DroneController::emergency(){
    deviceController->aRDrone3->sendPilotingEmergency(deviceController->aRDrone3);
}

void DroneController::stop(){
    deviceController->aRDrone3->setPilotingPCMD(deviceController->aRDrone3, 0, 0, 0, 0, 0, 0);
}

void DroneController::takeOff(){
    deviceController->aRDrone3->sendPilotingTakeOff(deviceController->aRDrone3);
}

void DroneController::land(){
    deviceController->aRDrone3->sendPilotingLanding(deviceController->aRDrone3);
}

int DroneController::takeImage(){
    if(videoCaptureOn){
        return 1;
    }else{
        deviceController->aRDrone3->sendMediaRecordPicture(deviceController->aRDrone3, 0);
        return 0;
    }
}

void DroneController::downloadImage(std::string dest){
    nsFTP::CFTPClient ftpClient;
    nsFTP::CLogonInfo logonInfo("192.168.42.1", 21, "anonymous", "");

    // connect to server
    ftpClient.Login(logonInfo);

    // get directory listing
    nsFTP::TFTPFileStatusShPtrVec list;
    ftpClient.List("internal_000/Bebop_2/media", list);

    // iterate listing
    for( nsFTP::TFTPFileStatusShPtrVec::iterator it=list.begin(); it!=list.end(); ++it ){
        if ( (*it)->Name().substr( (*it)->Name().find_last_of(".") + 1 ) == "jpg" ){
            remove(dest.c_str()); // remove any existing instance of the file, since ftpClient.DownloadFile does not overwrite (I think)
            pthread_mutex_lock(&mutex_global_image);
            ftpClient.DownloadFile("internal_000/Bebop_2/media/" + (*it)->Name(), dest);
            pthread_mutex_unlock(&mutex_global_image);
            ftpClient.Delete("internal_000/Bebop_2/media/" + (*it)->Name());
        }
    }
    
    // clean up
    ftpClient.Logout();
    std::cout << "Download complete!" << std::endl;
}

int DroneController::takeAndDownloadImage(std::string dest){
    if(videoCaptureOn){
        return 1;
    }else{
        pthread_mutex_lock(&mutex_takeImage);
        takeImage();
        sem_wait(&sem_takeImage);
        downloadImage(dest);
        pthread_mutex_unlock(&mutex_takeImage);
        return 0;
    }
}

void DroneController::toggleVideoCapture(){
    if(videoCaptureOn){
        deviceController->aRDrone3->sendMediaRecordVideoV2(deviceController->aRDrone3, ARCOMMANDS_ARDRONE3_MEDIARECORD_VIDEOV2_RECORD_STOP);
    }else{
        pthread_mutex_lock(&mutex_takeImage);
        deviceController->aRDrone3->sendMediaRecordVideoV2(deviceController->aRDrone3, ARCOMMANDS_ARDRONE3_MEDIARECORD_VIDEOV2_RECORD_START);
        sem_wait(&sem_activateVideoCapture);
        pthread_mutex_unlock(&mutex_takeImage);
    }
}

void DroneController::downloadAll(std::string dest){
    pthread_t download_thread;
    pthread_create(&download_thread, NULL, &downloadAll_func, (void *)&dest);
}

void *DroneController::downloadAll_func(void * arg){
    std::string dest = *((std::string *) arg);
    
    nsFTP::CFTPClient ftpClient;
    nsFTP::CLogonInfo logonInfo("192.168.42.1", 21, "anonymous", "");

    // connect to server
    ftpClient.Login(logonInfo);

    // get directory listing
    nsFTP::TFTPFileStatusShPtrVec list;
    ftpClient.List("internal_000/Bebop_2/media", list);

    // iterate listing
    for( nsFTP::TFTPFileStatusShPtrVec::iterator it=list.begin(); it!=list.end(); ++it ){
        ftpClient.DownloadFile("internal_000/Bebop_2/media/" + (*it)->Name(), dest + (*it)->Name());
    }
    
    // clean up
    ftpClient.Logout();
    std::cout << "Download complete!" << std::endl;
}

void DroneController::clearMemory(){
    nsFTP::CFTPClient ftpClient;
    nsFTP::CLogonInfo logonInfo("192.168.42.1", 21, "anonymous", "");

    // connect to server
    ftpClient.Login(logonInfo);

    // get directory listing
    nsFTP::TFTPFileStatusShPtrVec list;
    ftpClient.List("internal_000/Bebop_2/media", list);

    // iterate listing
    for( nsFTP::TFTPFileStatusShPtrVec::iterator it=list.begin(); it!=list.end(); ++it ){
        ftpClient.Delete("internal_000/Bebop_2/media/" + (*it)->Name());
    }
    
    // clean up
    ftpClient.Logout();
}

void DroneController::move(double x, double y, double z){
    // THIS FUNCTION DOES NOT WORK. DRONE DOES NOTHING IF SENT THIS COMMAND.
    int8_t  roll    = (int8_t) x, // right strafe (vector forward)
            pitch   = (int8_t) y, // forward (vector to the left)
            gaz     = (int8_t) z; // upward (translatory motion, vector up)
          //yaw     = 0;         // rotation clockwise (vector down)
    deviceController->aRDrone3->setPilotingPCMD(deviceController->aRDrone3, 1, roll, pitch, 0, gaz, 0);
}

void DroneController::turn(double angle){
    deviceController->aRDrone3->setPilotingPCMDYaw(deviceController->aRDrone3, (int8_t)-angle);
}

void DroneController::moveBy(double x, double y, double z){
    deviceController->aRDrone3->sendPilotingMoveBy(deviceController->aRDrone3, (float)y, (float)x, -(float)z, 0);
}
void DroneController::turnBy(double angle){
    deviceController->aRDrone3->sendPilotingMoveBy(deviceController->aRDrone3, 0, 0, 0, (float)-angle);
}

void DroneController::roll(double x){
    deviceController->aRDrone3->setPilotingPCMDRoll(deviceController->aRDrone3, x);
    deviceController->aRDrone3->setPilotingPCMDFlag(deviceController->aRDrone3, 1);
}

void DroneController::pitch(double y){
    deviceController->aRDrone3->setPilotingPCMDPitch(deviceController->aRDrone3, y);
    deviceController->aRDrone3->setPilotingPCMDFlag(deviceController->aRDrone3, 1);
}

void DroneController::gaz(double z){
    deviceController->aRDrone3->setPilotingPCMDGaz(deviceController->aRDrone3, z);
}