function M = getTransformationMatrix(a, b)
    % This function builds a transformation matrix
    % that takes the vector a to the vector b.
    % Vectors a and b should be columns. M is 4x4.
    a = a(1:3);
    b = b(1:3);
    u  = cross(a,b); % rotation vector
    u = u/norm(u);
    v = acos(dot(a,b)/norm(a)/norm(b)); % rotation angle
    crossMatrix = [0,-u(3),u(2);u(3),0,-u(1);-u(2),u(1),0];
    R = eye(3)*cos(v) + crossMatrix*sin(v) + (1-cos(v))*u*u'; % rotation matrix
    s = norm(b)/norm(a); % scale factor
    M = [R, zeros(3,1);0,0,0,1]*s;
end