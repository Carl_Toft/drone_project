
#include "droneController.h"
#include "locator.h"
#include "navigator.h"
// #include "pathPlanner.h"
#include "userInterface.h"
#include "global.h"
#include <iostream>
#include <cstdlib>
#include <time.h>
#include <pthread.h>
#include <iostream>

using namespace std;

int main(void){
    
    // Seed random number generator
    srand(time(0));
    
    // ******** Declare objects ********
    //DroneController droneController; // created in global scope
    Locator locator;
    Navigator navigator;
    UserInterface userInterface;
    
    // ******** Set bindings and interfaces between objects ********
    droneController.setLocator(&locator);
    droneController.setNavigator(&navigator);
    droneController.setUserInterface(&userInterface);
    
    locator.setDroneController(&droneController);
    locator.setUserInterface(&userInterface);
    
    userInterface.setDroneController(&droneController);
    userInterface.setNavigator(&navigator);
    userInterface.setLocator(&locator);
    
    navigator.setDroneController(&droneController);
    navigator.setLocator(&locator);
    navigator.setUserInterface(&userInterface);
        
    // ******** Run setup for classes that need it ********
    mutex_global_image = PTHREAD_MUTEX_INITIALIZER;
    
    if(droneController.start()){
        cout << "Failed to start the drone controller." << endl;
    }
    if(userInterface.start()){
        cout << "Failed to start the user interface." << endl;
    }
    if(locator.setup()){
        cout << "Failed to setup the locator." << endl;
    }
    
    // ******** Enter main loop ********
    while(mainLoopRunning){
        usleep(50);
    }
    
    // ******** Joint threads and shut down graciously ********
    userInterface.close();
    navigator.close();
    locator.close();
    droneController.close();
    pthread_mutex_destroy(&mutex_global_image);
    
    return 0;
}