import subprocess

proc = subprocess.Popen(["ls", "image_raw"], stdout=subprocess.PIPE)
filenames = proc.stdout.read().split()

i = 0

for f in filenames:
	i = i +1
	if i < 10:
		num = "0"+str(i)
	else:
		num = str(i)
	subprocess.call(["cp", "image_raw/"+f, "image_new/image"+num+".jpg"])
