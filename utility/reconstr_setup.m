function settings = reconstr_setup
% This is the reconstruction settings for idrottshall_v3

%Load calibration matrix (same calibration for all cameras at present)
load Calib_Results.mat KK
settings.KK = KK;
settings.kc = zeros(5,1);

%Select the images
settings.img_path = '/home/mattias/Documents/drone_media/idrottshall_v3/extracted_images/';
settings.imnames = dir(strcat(settings.img_path,'*.jpg'));

settings.expectedF = [];

settings.rotationvariantSIFT = 0;
settings.PeakThresh = 1;
settings.EdgeThresh = 10;
                       
                       
%path to where to save results
settings.save_path = '/home/mattias/programs/SfM/SfM_lib/idrottshall_v3/';

%Path to Lowes SIFT-implementation
settings.SIFT_path = '/home/mattias/programs/SfM/siftdemov4/';

%Setup VLfeat.
fold = pwd;
cd /home/mattias/programs/SfM/vlfeat-0.9.13/toolbox/
vl_setup
cd(fold);

%Rescales the images to speed up SIFT.
settings.scale = 0.5;

% Same criterion as Lowe = 0.5 
settings.distRatio = 0.5; 

%RANSAC_threshold
settings.RANSAC_pixtol = 2.5; %Tolerans vid RANSAC-k�rning

%Minimum number matches to compute two-view geometris
settings.mininlnr = 20;

settings.storesift = 1; %save sift correspondences?

%Minimum number of inliers to trust two-view results
settings.mincorrnr = 20;

%Threshold for inlier (in rotation averaging)
settings.roterrtol = 0.1; 

%Tolerans for first known-rotation run
settings.pixtol1 = 5; 

%Tolerans for second known-rotation run (don't know why I have two of
%these)
settings.pixtol2 = 5; 

%Points should be seen in at least this many cameras to be included in the
%optimization (to save memory).
settings.visviews = 3;

%Points in the final reconstruction that are uncertain in depth are removed
%after the reconstruction.
settings.uncertin_tol = settings.pixtol1; %Problems with the scale ambiguity?

%camera graph can be used to single out images to be matched if the
%sequence is not unordered.
n = length(settings.imnames);
settings.camera_graph = ones(n);
%settings.camera_graph = zeros(n); 
%num_adjacent_photos_for_matching = 4; % only match a photo with the closest adjacent ones
%for i = 1:n
%    for j = (i):(i+num_adjacent_photos_for_matching-1)
%        settings.camera_graph(i,1+mod(j,n)) = 1;
%    end
%end
%settings.camera_graph = settings.camera_graph + settings.camera_graph'; % fill lower diagonal of camera graph too 

%Has something to do with the point tracking. Sould probably alwasys be 1.
settings.merge_tracks = 1;

settings.forbidden={};
settings.epipoledistance = 150; %remove points around expected epipole
