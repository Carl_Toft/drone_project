
#include "getMatches.h"
#include <Eigen/Dense>
#include <iostream>
#include <stdint.h>

using namespace Eigen;
using namespace std;

int main (){
    
    MatrixXd coordImage_in;
    MatrixXd coordCloud_in;
    
    MatrixXd coordImage_out;
    MatrixXd coordCloud_out;
    
    MatrixXd descImage;
    MatrixXd descCloud;
    
    VlKDForest* kdtree;
    
//     tsvToMatrix ("../matlab_sift/coordinates.tsv", &coordCloud_in, 3);
//     tsvToMatrix ("../matlab_sift/descriptors.tsv", &descCloud, 128);
    
    tsvToMatrix ("../matlab_sift/coordCloud.tsv", &coordCloud_in, 4);
    tsvToMatrix ("../matlab_sift/descCloud.tsv", &descCloud, 128);
    
//     tsvToMatrix ("coord.tsv", &coordCloud_in, 3);
//     tsvToMatrix ("desc.tsv", &descCloud, 128);
    
//     tsvToMatrix ("coord.tsv", &coordImage_in, 3);
//     tsvToMatrix ("desc.tsv", &descImage, 128);
    
//     cout << endl << coordCloud_in.cols() << endl;
    
    initializeKDTree (&kdtree, &descCloud);
    
    getSiftPoints ("images/DSC_0025.PGM", &coordImage_in, &descImage);
    
//     matrixToTsv ("coord.tsv", &coordImage_in);
//     matrixToTsv ("desc.tsv", &descImage);
    
    matchSiftPoints (kdtree, &descImage, &descCloud, &coordImage_in, &coordCloud_in, &coordImage_out, &coordCloud_out);
    
    matrixToTsv ("corCoordImage.tsv", &coordImage_out);
    matrixToTsv ("corCoordCloud.tsv", &coordCloud_out);
    
    // DEBUG
    cout << endl << coordImage_in.cols() << endl;
    cout << endl << coordImage_out.cols() << endl;
    
    return 0;
}