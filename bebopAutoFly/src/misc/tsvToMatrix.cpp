// #include "getMatches.h"
#include "tsvToMatrix.h"
#include <Eigen/Dense>
#include <vector>
#include <fstream>
#include <string>

using namespace std;

int tsvToMatrix (string fileName, Eigen::MatrixXd& matrix, const int nRows){
    ifstream inFile;
    string line;
    string entry;
    const char delim = '\t';
    
    vector<double> matrix_vec;
    
    int nCols = 0;
    
    // Read the values into a vector
    inFile.open(fileName);
    if(!inFile.is_open()) return 1;
    while (getline(inFile, line)){
        stringstream strStream(line);
        while(getline(strStream, entry, delim)){
            matrix_vec.push_back( stod(entry) );
        }
    }
    inFile.close();

    nCols = matrix_vec.size() / nRows;
    
    // OBS Reading straight from file should render row major order! Fix this with stride or transpose!
    
    // Mapping the vectors to matrices
    matrix = Eigen::MatrixXd::Map(&matrix_vec[0], nCols, nRows); // OBS dimensions swapped
    
    // Transposing the matrices to get back to column major order
    matrix.transposeInPlace();
    
    return 0;
}

int matrixToTsv (string fileName, Eigen::MatrixXd& matrix){
    ofstream outFile;
     
    outFile.open(fileName);
    
    if(!outFile.is_open()) return 1;
    
    for (int i = 0; i < matrix.rows(); i++){
        for (int j = 0; j < matrix.cols(); j++){
            outFile << matrix(i,j);
            if (j == matrix.cols()-1){
                outFile << "\n";
            }else{
                outFile << "\t";
            }
        }
    }
    
    outFile.close();

    return 0;
}