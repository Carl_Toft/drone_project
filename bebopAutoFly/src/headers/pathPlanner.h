// #ifndef _PATH_PLANNER
// #define _PATH_PLANNER
// 
// #include "manualController.h"
// #include "navigator.h"
// #include "visualizer.h"
// 
// class PathPlanner{
//     private:
//         ManualController manualController;
//         Navigator navigator;
//         Visualizer visualizer;
//         
//     public:
//         PathPlanner();
//         ~PathPlanner();
//         
//         void setManualController(ManualController & m);
//         void setNavigator(Navigator & n);
//         void setVisualizer(Visualizer & v);
// };
// 
// #endif