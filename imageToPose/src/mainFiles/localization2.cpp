#include "p3p.h" 
#include <iostream>
#include <Eigen/Eigen>
#include "getMatches.h"

using namespace std;

Eigen::MatrixXd cameraPoseRANSAC(const Eigen::MatrixXd& impoints, const Eigen::MatrixXd& spacePoints, unsigned int numIterations, double tol);

int main(){
    
    Eigen::MatrixXd coordImage;
    Eigen::MatrixXd coordCloud;
    Eigen::MatrixXd calib;
    
    tsvToMatrix ("corCoordImage.tsv", &coordImage, 3);
    tsvToMatrix ("corCoordCloud.tsv", &coordCloud, 4);
    tsvToMatrix ("calibrationMatrix.tsv", &calib, 3);
    
    coordImage = calib.inverse() * coordImage;
    
    // Norm the columns of the image matrix
    for(int i = 0; i < coordImage.cols(); i++){
        double invnorm = 1/coordImage.col(i).norm();
        coordImage.col(i) = coordImage.col(i) * invnorm;
    }
    
    // Remove the forth row of the space matrix
    coordCloud.conservativeResize(3,coordCloud.cols());
    
//     cout << coordCloud.rows() << "\t" << coordCloud.cols() << endl;
    
    Eigen::MatrixXd p = cameraPoseRANSAC(coordImage, coordCloud, 100, 0.002);
    
    cout << p << endl;
    
    return 0;
}