#include "getMatches.h"
#include "cameraPose.h"
#include <Eigen/Dense>
#include <iostream>

using namespace Eigen;
using namespace std;

int main (){
    
    int err; // error code
    
    MatrixXd coordImage_in;
    MatrixXd coordCloud_in;
    
    MatrixXd coordImage_out;
    MatrixXd coordCloud_out;
    
    MatrixXd descImage;
    MatrixXd descCloud;
    
    MatrixXd calib;
    
    VlKDForest* kdtree;
    
    // Read data (cloud data and calibration matrix)
    err = 0;
    err += tsvToMatrix ("data/coordCloud.tsv", coordCloud_in, 4);
    err += tsvToMatrix ("data/descCloud.tsv", descCloud, 128);
    err += tsvToMatrix ("data/calibrationMatrix.tsv", calib, 3);
    
    if(err){
        cout << "Failed to read one or more files. Terminating program." << endl;
        return 1;
    }
    
    normColumns(descCloud);
    
    // Construct KDTree from cloud data
    initializeKDTree (kdtree, descCloud);
    
    // Get SIFT-points from the current image
    if( getSiftPoints ("data/DSC_0025.PGM", coordImage_in, descImage) ){
        cout << "Failed to extract SIFT-points. Terminating program." << endl;
        return 1;
    }
    
    normColumns(descImage);
    
    // Match the image points to the cloud points
    matchSiftPoints (kdtree, descImage, descCloud, coordImage_in, coordCloud_in, coordImage_out, coordCloud_out);
    
    // Fix the coordinate matrices
        // Apply inverse camera matrix to the image points
    coordImage_out = calib.inverse() * coordImage_out;
        // Norm the columns of the image point matrix
    normColumns(coordImage_out);
        // Remove the forth row of the space matrix
    coordCloud_out.conservativeResize(3,coordCloud_out.cols());
    
    // Compute the camera pose from the image-cloud coordinate correlations
    MatrixXd p = cameraPoseRANSAC(coordImage_out, coordCloud_out, 100, 0.002);
    
    // Print the result
    cout << p << endl;
    
    return 0;
}
