#ifndef _LOCATOR
#define _LOCATOR

#include "droneController.h"
// #include "navigator.h"
#include "userInterface.h"
#include "misc.h"
#include <pthread.h>
#include <semaphore.h>
#include <string>
#include <Eigen/Dense>

extern "C"{
    #include <vl/kdtree.h>
}

// Forward declarations
class DroneController;
// class Navigator;
class UserInterface;

enum LOCATOR_TASK_t {
    LOCATOR_TASK_ONE_SHOT_IMAGE_LOCALIZATION,
    LOCATOR_TASK_CONTINUOUS_IMAGE_LOCALIZATION,
    LOCATOR_TASK_NONE
};

class Locator{
    private:
        DroneController * droneController;
        UserInterface * userInterface;
        
        LOCATOR_TASK_t taskToStart;
        pthread_t thread;
        bool threadCreated;
        static void *thread_starter(void *context); // helper function to make a thread use a member-function
        
        std::string dataDir;
        std::string imageNamePGM;
        std::string imageNameJPG;
        
        Pose pose;
        Pose poseImage;
        Pose poseIMU;
        Pose poseIMUByLastImagePose;
        Pose poseIMUByImageUnderProcessing;
        pthread_mutex_t mutex_pose;
        pthread_mutex_t mutex_poseIMU;
        pthread_mutex_t mutex_poseImage;
        
        double scale;
        int scaleUpdates;
        
        Eigen::MatrixXd coordCloud_in;
        Eigen::MatrixXd descCloud;
        Eigen::MatrixXd calib;
        VlKDForest* kdtree;
        unsigned int inlierThreshold;
        
        bool continuousImageLocalizationRunning;
        
        void oneShotImageLocalization();
        void continuousImageLocalization();
        
        int takeNewImage();
        int updatePoseImage();
        
    public:
        Locator();
//         ~Locator();
        
        int setup();
        void close();
        
        void setDroneController(DroneController * d);
        void setUserInterface(UserInterface * u);
        
        void setScale(double s);
        double getScale();
        
        void getPose(Pose &poseToUpdate);
        void getPoseIMU(Pose &poseToUpdate);
        void getPoseImage(Pose &poseToUpdate);
        
        bool getContinuousImageLocalizationRunning();
        
        int startOneShotImageLocalization(Pose& poseToUpdate);
        int startContinuousImageLocalization();
        void stopContinuousImageLocalization();
        void stopContinuousImageLocalization_blocking();
        void toggleContinuousImageLocalization();
        
        void updateCloud(Eigen::Matrix<double, 4, 4> transformationMatrix); // TODO
        
        void updatePoseIMUPosition(double speedX, double speedY, double speedZ, double dTime);
        void updatePoseIMUHeading(double roll, double pitch, double yaw); // TODO fix conversion from rpy to vector
};

#endif