function color = formatPointCloudColor(u, U, average, settings)
% *** Load stuff ***
img_path = settings.img_path;
imnames = settings.imnames;
KK = settings.KK;
kc = settings.kc;
nbrOfImages = length(u.index);
nbrOfWorldPoints = size(U,2);

% *** Initialize the storage ***
color = zeros(3, nbrOfWorldPoints);
nbrOfKeyPoints = zeros(1, nbrOfWorldPoints);

% *** Gather the data ***
for i_image = 1 : nbrOfImages
    im = imread(strcat(img_path,imnames(i_image).name));
    x = u.points{i_image};
    x = KK*pextend(apply_distortion(x(1:2,:),kc));
    x = round(x(1:2,:));
    for i_impoint = 1 : length(u.index{i_image})
        i_worldpoint = u.index{i_image}(i_impoint);
        rgb = [ im(x(2, i_impoint), x(1, i_impoint), 1);
                im(x(2, i_impoint), x(1, i_impoint), 2);
                im(x(2, i_impoint), x(1, i_impoint), 3)];
        rgb = double(rgb);
        if average
            color(:, i_worldpoint) = color(:, i_worldpoint) + rgb;
            nbrOfKeyPoints(i_worldpoint) = nbrOfKeyPoints(i_worldpoint) + 1;
        else
            if nbrOfKeyPoints(i_worldpoint) == 0
                color(:, i_worldpoint) = rgb;
                nbrOfKeyPoints(i_worldpoint) = nbrOfKeyPoints(i_worldpoint) + 1;
            end
        end
    end
end
for i = 1 : size(color, 2)
    color(:, i) = color(:, i)./nbrOfKeyPoints(i);
end

% *** Write to files ***
dlmwrite('colorCloud.tsv', color, '\t');