#ifndef _NAVIGATOR
#define _NAVIGATOR

#include "droneController.h"
#include "locator.h"
#include "userInterface.h"
#include "misc.h"
// #include "pathPlanner.h"
#include <Eigen/Dense>
#include <pthread.h>
#include <semaphore.h>
#include <string>

class DroneController;
class Locator;
class UserInterface;

enum NAVIGATOR_SCHEMES_t {
    NAVIGATOR_SCHEME_NONE,
    NAVIGATOR_SCHEME_CALIBRATION_DANCE,
    NAVIGATOR_SCHEME_MOVE_TO_WAYPOINT,
    NAVIGATOR_SCHEME_SAVE_WAYPOINT,
    NAVIGATOR_SCHEME_CALIBRATION_DANCE_2
};

class Navigator{
    public:
        Navigator();
//         ~Navigator();
        
        void close();
        
        void setDroneController(DroneController * d);
        void setLocator(Locator * l);
        void setUserInterface(UserInterface * u);
        
        int startCalibrationDance();
        int startMoveToWaypoint();
        int startSaveWaypoint();
        
        void stop();
        void moveComplete(double dX, double dY, double dZ);
    
    private:
        DroneController * droneController;
        Locator * locator;
        UserInterface * userInterface;
//         PathPlanner pathPlanner;
        
        NAVIGATOR_SCHEMES_t schemeToStart;
        bool shouldStop;
        bool threadCreated;
        pthread_t thread;
        sem_t sem_moveComplete;
        
        Eigen::Vector3d reportedMove;
        
        std::string dataDir;
        std::string fileNameTransformation;
        std::string fileNameManualWaypoint;
        
//         void updatePoseEstimate(); // synchronize with locator, call locator
//         bool isMoveActuallyComplete();
//         void requestNextWaypoint(); // synchronize with pathPlanner, call pathPlanner
//         void computeNextMove();
//         void executeMove(); // call droneController
        
        static void *thread_starter(void *context); // helper function to make a thread use a member-function
        
        void calibrationDance();
        void calibrationDance2();
        void moveToWaypoint();
        void saveWaypoint();
        
        void measureVector(Eigen::Vector3d& u_image, Eigen::Vector3d& v_imu, Eigen::Vector3d& move);
        Eigen::Matrix4d getTransformationMatrix(Eigen::Vector3d& v_start, Eigen::Vector3d& v_goal);
        int waitForMoveComplete();
        double rotationAnglePlaneProj(Pose pose, Eigen::Vector3d vec);
};

#endif