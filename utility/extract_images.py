import sys
import os
import subprocess

nArgs = len(sys.argv)-1
print nArgs

if nArgs == 0:
    print "call like this: python extract_images.py <video_name> <path_to_video_dir> <frame_ratio>"
    sys.exit(0)
if nArgs >= 1:
    video_name = sys.argv[1]
    path_to_video = "."
    extraction_ratio = 30
if nArgs >= 2:
    path_to_video = sys.argv[2]
    extraction_ratio = 30
if nArgs == 3:
    extraction_ratio = sys.argv[3]

os.system("mkdir -p " + path_to_video + "/image_raw")

os.system("cvlc --video-filter scene -V dummy --scene-format='jpg' --scene-ratio="+str(extraction_ratio)+" --scene-prefix=test --scene-path="+path_to_video+"/image_raw "+path_to_video+"/"+video_name+" vlc://quit")

proc = subprocess.Popen(["ls", path_to_video+"/image_raw"], stdout=subprocess.PIPE)
filenames = proc.stdout.read().split()

if len(filenames) >= 1000:
    sys.exit("More than 1000 images, will not rename.")
else:
    os.system("mkdir -p " + path_to_video + "/extracted_images")

i = 0
    
if len(filenames) < 100:
    for f in filenames:
            i = i +1
            if i < 10:
                    num = "0"+str(i)
            else:
                    num = str(i)
            subprocess.call(["cp", path_to_video+"/image_raw/"+f, path_to_video+"/extracted_images/image"+num+".jpg"])
elif len(filenames) < 1000:
    for f in filenames:
	i = i +1
	if i < 10:
		num = "00"+str(i)
	elif i < 100:
		num = "0"+str(i)
	else:
		num = str(i)
	subprocess.call(["cp", path_to_video+"/image_raw/"+f, path_to_video+"/extracted_images/image"+num+".jpg"])

os.system("rm -r "+path_to_video+"/image_raw")
