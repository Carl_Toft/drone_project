#include "misc.h"
#include <Eigen/Eigen>
#include <string>

Pose::Pose(){
    position = Eigen::Vector3d::Zero();
    heading = Eigen::Vector3d::Zero();
    cameraMatrix = Eigen::Matrix<double, 3, 4>::Zero();
}

Pose::Pose(Eigen::Vector3d posIn, Eigen::Vector3d headIn){
    position = posIn;
    heading = headIn;
    cameraMatrix = Eigen::Matrix<double, 3, 4>::Zero();
}

Pose::Pose(double ix, double iy, double iz, double ipax, double ipay, double ipaz){
    position(0) = ix;
    position(1) = iy;
    position(3) = iz;
    heading(0) = ipax;
    heading(2) = ipay;
    heading(3) = ipaz;
    cameraMatrix = Eigen::Matrix<double, 3, 4>::Zero();
}

void Pose::updatePose(Pose &newPose){
    position = newPose.position;
    heading = newPose.heading;
    cameraMatrix = newPose.cameraMatrix;
}

void Pose::updateWithCameraPose(Eigen::MatrixXd & cameraPose){
    if(cameraPose.rows() == 3 && cameraPose.cols() == 4){
        position = - cameraPose.block<3,3>(0,0).transpose() * cameraPose.col(3);
        heading = cameraPose.block<1,3>(2,0).transpose();
        cameraMatrix = cameraPose;
    }
}

double Pose::distance(Pose &otherPose){
    Eigen::Vector3d diff = otherPose.position - position;
    return diff.norm();
}

std::string Pose::toString(){
    std::string s = std::to_string(position(0)) + "\t" + std::to_string(position(1)) + "\t" + std::to_string(position(2))
    + "\t" + std::to_string(heading(0)) + "\t" + std::to_string(heading(1)) + "\t" + std::to_string(heading(2));
    return s;
}