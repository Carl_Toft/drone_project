#include "getMatches.h"
#include "cameraPose.h"
#include <Eigen/Dense>
#include <iostream>
#include <string>

using namespace Eigen;
using namespace std;

int main (){
    
    int err; // error code
    
    MatrixXd coordImage_in;
    MatrixXd coordCloud_in;
    
    MatrixXd coordImage_out;
    MatrixXd coordCloud_out;
    
    MatrixXd descImage;
    MatrixXd descCloud;
    
    MatrixXd calib;
    
    VlKDForest* kdtree;
    
    string dataDir = "data5/";
    string imageJPG = "image.jpg";
    string image = "image.pgm"; // DSC_0025, DSC_0029.PGM, DSC_0032.PGM, image22.pgm, image10.pgm
    
    std::string convertCall = "magick convert " + dataDir + imageJPG + " -colorspace gray -depth 8 " + dataDir + image;
    system(convertCall.c_str());
    
    // Read data (cloud data and calibration matrix)
    err = 0;
    err += tsvToMatrix (dataDir + "coordCloud.tsv", coordCloud_in, 4);
    err += tsvToMatrix (dataDir + "descCloud.tsv", descCloud, 128);
    err += tsvToMatrix (dataDir + "calibrationMatrix.tsv", calib, 3);
    
    if(err){
        cout << "Failed to read one or more files. Terminating program." << endl;
        return 1;
    }
    
    normColumns(descCloud);
    
    // Construct KDTree from cloud data
    initializeKDTree (kdtree, descCloud);
    
    // Get SIFT-points from the current image
    if( getSiftPoints (dataDir + image, coordImage_in, descImage) ){
        cout << "Failed to extract SIFT-points. Terminating program." << endl;
        return 1;
    }
    
    normColumns(descImage);
    
    // Match the image points to the cloud points
    matchSiftPoints (kdtree, descImage, descCloud, coordImage_in, coordCloud_in, coordImage_out, coordCloud_out);
    
    // Fix the coordinate matrices
        // Apply inverse camera matrix to the image points
    coordImage_out = calib.inverse() * coordImage_out;
        // Norm the columns of the image point matrix
    normColumns(coordImage_out);
        // Remove the forth row of the space matrix
    coordCloud_out.conservativeResize(3,coordCloud_out.cols());
    
    // Compute the camera pose from the image-cloud coordinate correlations
    unsigned int numInliers = 0; 
    MatrixXd p = cameraPoseRANSAC(coordImage_out, coordCloud_out, 100, 0.002, numInliers); // params: 100, 0.002
        
    // Save the result
    matrixToTsv (dataDir + "cameraMatrix.tsv", p);
    
    std::cout << "number of keypoints: " << coordImage_in.cols() << std::endl; // DEBUG
    std::cout << "number of matches: " << coordImage_out.cols() << std::endl; // DEBUG
    std::cout << "number of inliers: " << numInliers << std::endl; // DEBUG
    std::cout << "********************************" << std::endl; // DEBUG
    
    // Text output
//     cout << "inliers:\t" << numInliers << endl;
    cout << p << endl;
    
    return 0;
}
