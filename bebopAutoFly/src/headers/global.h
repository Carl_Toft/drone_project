#ifndef _GLOBAL
#define _GLOBAL

#include "droneController.h"
#include <pthread.h>

class DroneController; // forward declaration

extern DroneController droneController;

extern bool mainLoopRunning;

extern pthread_mutex_t mutex_global_image;

#endif