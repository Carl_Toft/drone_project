\documentclass[12pt,a4paper]{article}
\usepackage[utf8]{inputenc}
\usepackage[swedish]{babel}
\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{placeins}
\usepackage{lscape}
\usepackage{lipsum}
\usepackage[margin=2.7cm]{geometry}

\newcommand{\HRule}{\rule{\linewidth}{0.5mm}}

\title{\vspace{-2cm}Documentation for the drone navigation project software}
\author{Mattias Kjelltoft}
\date{\today}

\begin{document}

\maketitle
\thispagestyle{empty}

\pagenumbering{roman}
\tableofcontents
\newpage

\section{Overview of the project}
\pagenumbering{arabic}

In this project software was developed with the goal of making a quadcopter drone fly autonomously, navigating using a camera and computer vision in order to localize itself relative to a 3D map. In order to do this four tasks are addressed:

\begin{itemize}
 \item Calibrating the camera
 \item Creating a 3D map
 \item Visualize the map with the drone in it
 \item Controlling the drone
\end{itemize}

This document will describe how the different programs created in this project to complete these tasks are designed and how to use them.

\section{Calibrating the camera}
A good calibration of the drones camera is required for everything in this project. To measure the camera calibration, use the matlab toolkit TOOLBOX\_calib from Caltech. Their website have good instructions, simply follow them. The results should be saved in a file called {\it Calib\_Results.mat}.

\section{Creating a 3D map}

The 3D map pipeline consist of several steps, using several separate programs.

\subsection{Capture a video of the scene}
In order to create a 3D model many images of the scene is needed. The easiest way to do this is to capture a video of the scene.

For best results the camera should move slow and steady without sudden jerks and capture all of the scene. The video should preferably capture the scene from approximately the same angles that the drone will look from later, even though this is not completely necessary. The scene should preferably be captured from a relatively close distance, as more details are then captured. The camera should preferably move in a closed circuit, ending up in the same pose it started, since this will reduce the risk of a skewed map.

Some scenes work better than others. For instance; big homogeneous surfaces like concrete walls, transparent or reflective objects like windows and environments with very fine structure like vegetation is not reconstructed well. Contrast rich environments with structure of medium size like brick walls or a messy desk is reconstructed well.

The easiest way of capturing the video material for the 3D map is for the moment to use the drones camera. Use the {\it bebopAutoFly} program, clear the drones memory, toggle video capture, then download the data.

\subsection{Extract images from the video}
In order to extract images from the video VLC was used. The python script {\it extract\_images.py} is a wrapper for the command to do so, but it also names the images conveniently. It is recommended to review the extracted images and remove any blurred images from the beginning and end of the series. After doing so, use the python script {\it rename\_sub100.py} or {\it rename\_sub1000.py} to rename the images accordingly.

More images generally makes the 3D reconstruction better, but it takes longer to run the software. In the project, usually a couple of hundred images was used, giving a computation time of the reconstruction of about an hour.

\subsection{Creating a 3D reconstruction of the scene}
This is the main step in creating a 3D model of the scene. Here the structure from motion software SfM\_lib was used, which was provided by the Image Analysis Group at Chalmers. This software have several dependencies, which are described in more detail in section \ref{sec:dep}.

First create a new directory SfM/SfM\_lib/$<$new dir$>$ for the new reconstruction. Place the calibration file {\it Calib\_Results.mat} and the reconstruction settings file {\it reconstr\_setup.m} in the new directory. Configure the settings file. Then start the reconstruction with {\it reconstruct.m}.

The important configurations to make in the settings file is to set the image path and save path. The image path should be the location of the images and the save path should be the newly created reconstruction directory.

One notable choice in the settings file is rotationvariantSIFT. It is very important that the SIFT variant used is rotation invariant, since that is the one used in the other programs.

% TODO describe how the reconstructed points are many-to-one and is merged by simply taking their mean for SIFT-descriptors.

\subsection{Formatting the 3D reconstruction data into a point cloud}
In this step we extract data from the reconstruction results and format them into a point cloud format, consisting of one file with coordinates, one file with SIFT descriptors and one file with color data. We also copies the camera calibration file. Everything is done by running the script {\it cloudPipeline.m} after supplying it with the reconstruction directory in the SfM\_lib directory and everything we need is outputted to a results directory. All the files in the results directory should then be copied to bebopAutoFly/data for use in the drone control program.

\section{Visualization}

The visualization program {\it pclVisualize} shows the point cloud data with point color extracted from the images and the pose of the drone in the form of a white cone. The visualization software is a separate program from the drone control software and may or may not be run together with it. The data for {\it pclVisualize} should be located in the directory bebopAutoFly/data.

To run {\it pclVisualize} simply run the the executable, which is located in visualization/build.

\section{Controlling the drone}

\subsection{Overview of the program}

The main objective of this project have been to make a drone able to localize itself and navigate autonomously using computer vision, but in order to enable the development of this goal feature many support functions have been created. The program {\it bebopAutoFly} is the result of this development process and is the main result of this project.

The program is structured in several classes which represents the tasks that are being solved.

\begin{itemize}
 \item DroneController -- the interface to the drone, passing and receiving commands, messages and data
 \item FtpClient -- used in droneController to download data from the drone and manipulate its storage drive
 \item Locator -- the computer vision algorithms that are used to locate the drone in relation to the point cloud
 \item Navigator -- the algorithms that govern the autonomous flight of the drone and turn point cloud positions into flight vectors
 \item UserInterface -- the user interface of the program which both displays important data for the operator on the screen and parses the keyboard signals. This can in some sense be seen as the main thread of the program.
\end{itemize}

Since this is a real time system with a potentially dangerous flying object it is most important that no process is blocking any other important process. Therefore most of the time when new tasks are to be handled a new thread is spawned for that task. The program has been written so that the operator should always be able to intervene and take manual control of the drone. If any key is pressed at any time the drone will immediately go into manual control mode and stop.

\subsection{Using the program}
To use the {\it bebopAutoFly} program, first you must start the drone and connect to its wifi network. Then you run the {\it bebopAutoFly} executable. The graphical user interface will appear with instructions on what commands can be sent to the drone and which keys the commands are bound to. The GUI also contains a window with the last image captured by the drone.

To launch the drone into flight, press {\it takeoff} and to land press {\it land}. The drone will manage the takeoff and landing autonomously. After takeoff the drone is in manual mode and will hover in place waiting for orders. You can control it with the arrow and wasd keys. If any key is pressed during any autonomous flight mode the drone will immediately stop and return to manual mode.

If the key {\it emergency} is pressed the drone will immediately cut the power to the motors and drop to the ground.

Continuous localization can be toggled with {\it toggle image localization}. The drone will then take a new image as soon as it is done with the localization of the last. The last image will appear in the GUI.

To use the drone as a camera for capturing video for the 3D map construction pipeline you should first use {\it clear memory}, then {\it toggle video capture} before and after capturing the environment and then press {\it download all}. This does not require the drone to be flying, video and images can be captured before takeoff.

To use the drones autonomous navigation functionality the bebopAutoFly/data directory must contain the files {\it coordCloud.tsv}, {\it descCloud.tsv} and {\it calibrationMatrix.tsv}. When you have started the drone and the {\it bebopAutoFly} program you must calibrate the 3D map before you use it for navigation. This is done by pressing {\it start calibration dance} while airborne. The drone will then fly one meter to the right in order to measure the scale of the cloud. After calibration the target pose should be set by positioning the drone in a satisfactory pose and pressing {\it save waypoint}. After this, the drone should be able to return back to the target pose when ordered so by pressing {\it move to waypoint}. Beware that if the drone fail to find its position with the computer vision localization algorithm at any point during autonomous flight it will stop and return to manual mode in order to maintain safety.

\subsection{Interfacing with the drone}
The class DroneController is a wrapper around the ParrotSDK api that interface with the drone and handles all commands, messages and data that is passed between the done and the computer. This is the only class in the program that handles direct drone communication, which means that if the type of drone would be changed in the future the integration of the new drone into the {\it bebopAutoFly} program would be easier.

The communication with the drone is done using ParrotSDK (sometimes referred to as ARDroneSDK3 API), a C api developed by the company Parrot for communicating with and controlling their drone products.

Images and videos is retrieved from the drone using ftp-download. The ParrotSDK can do this, but I found it easier to use specialized third party ftp client code

(https://www.codeproject.com/Articles/8667/FTP-Client-Class).

The DroneController class sets up communication with the drone and then mostly passes commands to it, giving access to a selection of the drones features through its own interface. In some places, like with the ftp-downloads, it does more than piping messages and have some actual logic. In the case with the ftp-downloads this logic consist of waiting for the image to be reported as taken before trying to download it.

\subsection{Localization}
The Locator class handles the localization algorithms of the drone. It was originally built for using image localization together with IMU integration, since the two methods of localization in theory should complement each other well. The IMU provides fast movement updates and is good for small adjustments but tend to drift over extended periods of time whereas the image localization is slower but provides a globally stable position. However, it was discovered that it was hard to calibrate the IMU integration well and fuse its data with the point cloud of the image localization. One problem was that the interface with the specific drone used in the project did not provide direct access to the IMU-sensor and it is possible that the IMU integration method could have been made to work better with such access. Therefore the IMU-integration was abandoned in favor of only using image localization, even though the code for IMU data processing was left intact for any future reattempts.

The image localization is based on projective geometry. When the localization function {\it updatePoseImage} is called the following steps are taken:

\begin{enumerate}
 \item An image is captured.
 \item The image is converted to the PGM gray-scale format.
 \item SIFT-points are extracted from the image.
 \item The SIFT-points is matched to the points of the cloud, using a KDTree data structure for improved speed.
 \item Camera calibration and normalization is applied to the matched points and the extra homogeneous coordinate is removed.
 \item The pose of the camera is computed with the RANSAC/p3p algorithm.
 \item The resulting pose is accepted or discarded based upon the number of inliers compared to a threshold.
\end{enumerate}

The coordinate and descriptor data of the point cloud should be stored in the bebopAutoFly/data directory. The most important parameter to adjust for the image localization algorithm is the inlier threshold, but the parameters of the RANSAC algorithm could also be adjusted. The software dependencies of the Locator class is ImageMagick for the gray-scale conversion, VLFEAT for the SIFT algorithm and KDTree data structure and the RANSAC/p3p algorithms that was provided by Chalmers.

\subsection{Navigation}
The Navigator class contains the autonomous navigation modes of the program, that is {\it saveWaypoint}, {\it moveToWaypoint}, {\it calibrationDance} and {\it calibrationDance2}.

The autonomous navigation modes makes use of the ParrotSDK command {\it moveBy} which instead of setting the drones angle (and thus speed) instead moves according to a provided vector relative to the drones body-frame. One could for instance ask it to move two meters to the left. How this movement is achieved is unclear since it is not revealed by the Parrot company, but I strongly suspect that they use IMU integration since it works fine indoors.

There are two calibration methods present in the code, {\it calibrationDance} and {\it calibrationDance2}. The first version was developed when IMU integration localization was still being developed and the calibration method tries to measure both the rotation and scale of the point cloud compared to the real world. This was achieved by first flying one meter straight up, taking images both before and after the movement, and then flying one meter to the right, taking one last image. The images could then be used to gain two vectors in the cloud frame, one corresponding to the vertical real world vector and one to a horizontal one. The transformation matrix between the cloud and real world frames would then be stored and used during future navigation and localization. However, when the IMU integration was abandoned only the scale of the cloud needed calibration, so the calibration dance was simplified to {\it calibrationDance2} where only one jump to the right is made.

The main autonomous navigation mode is {\it moveToWaypoint}. When it is activated the drone tries to fly back to the pose it stored from when {\it saveWaypoint} was activated. It does this by making small jumps forward of maximum one meter towards the waypoint, stopping between them in order to make a new localization and update a new flight vector. When it is close enough, for the moment set to half a meter, it stops, adjust its yaw angle to match the waypoint and then reports done. In one sense this movement method is the same as the originally intended one with moving locally using IMU integration and doing periodical global updates with image localization, only here instead of developing the IMU integration myself I use the drones built in version.

Now some notes for anyone interested in developing this further in the future. There was an attempt at refining the scale calibration with every new jump during flight, but this feature was deactivated in order to reduce complexity during debugging and testing of the system. The parameters one could be interested in adjusting are the maximum jump distance, the distance from the waypoint within which the drone is considered arrived and the jump length of the calibration dance. The transformation of the flight vector is done in two steps, first from the cloud frame to the camera frame and then from the camera frame to the drone frame. The camera and drone frames are essentially the same, but with different permutations of the axes.

Some testing and measurements were made in order to evaluate the navigation. The measurements consisted of letting the drone try to find its way back to a waypoint many times and photographing its final position. The results from these measurements showed a bias error of approximately one meter and a variance of approximately half a meter, which is consistent with what could be roughly estimated by eyeballing the positions during the measurements. The bias can in part be explained by that the drone was released from the same direction most of the times. The numbers should be taken by a pinch of salt as they are based on the scale calibration from the calibration dance which is quite rough and noisy.

\subsection{The ParrotSDK}
The communication with the drone is done using ParrotSDK (sometimes referred to as ARDroneSDK3 API), a C api developed by the company Parrot for communicating with and controlling their drone products. The documentation found on the company's website (http://developer.parrot.com/docs/SDK3/) is quite good. They explain how to set up the api, they provide some sample projects and there is a reference list of all commands.

When I started working all but one of the sample projects where broken, but they seem to have cleaned that up by now. Today I can only find one sample project which is called BebopSample and that should work fine.
The DroneController class in the {\it bebopAutoFly} program should work quite good as a sample as well, since it contains all necessary code for setting up and using the the api. Personally I would even consider this code easier to understand since the DroneController is written with an object oriented mindset rather than the more traditional sequential, callback heavy style in the C samples from Parrot.

The commands used with the drone are very straight forward, such as ``take image'' or ``take off''. It should be easy to get an overview by looking at the list of functions int {\it droneController.cpp} and cross referencing with the Parrot documentation. One could note that the primary mode of controlling the drones movement is by setting the roll, pitch and yaw angles, but there is also a secondary mode of movement where one specifies a relative vector from the drones current pose (``move three meters forward and one meter up'' for instance).

The images and videos taken by the drone is stored locally on the drones flash drive and must be retrieved by ftp-download. The ParrotSDK api have some functionality for this, but I found it easier to use a separate api to handle the ftp. The drone can also streamed video, but the image quality was quite poor and anomalies where frequent enough that it was not very suitable for image processing.


\section{Dependencies}
\label{sec:dep}

\begin{itemize}
 \item ParrotSDK
 \item ImageMagick
 \item VLC
 \item PCL
 \item ToolboxCalib
 \item Python
 \item Matlab
 \item VLFeat
 \item Mosek
 \item Eigen
 \item SfMlib
 \item CVX
 \item Allegro
\end{itemize}


\end{document}