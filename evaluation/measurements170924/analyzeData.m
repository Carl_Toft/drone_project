%% LOAD THE DATA AND EXTRACT VALUES
clear

% provide som initial data
directory = 'measurements';
baseName = 'measurementImage';
suffix = 'jpg';
nImages = 21;
principalDist = 4.5; % meters

% load the focal length data from the camera calibration
load('calibration/Calib_Results.mat', 'fc')
focalLength = mean(fc);

% load images and manually get pixel positions for the drone
im = imread(sprintf('%s/%s.%s', directory, 'goal', suffix));
image(im);
[x, y] = ginput(1);
goalCoordPix = [x, y];

measurementCoordPix = zeros(nImages, 2);
for i=1:nImages
    im = imread(sprintf('%s/%s%02d.%s', directory, baseName, i, suffix));
    image(im);
    [x, y] = ginput(1);
    measurementCoordPix(i, :) = [x, y];
end

%% ANALYZE THE MEASUREMENT VALUES
% Compute the side ratio of the triangle formed by the camera center, the principal point and the image point
ratioGoal = goalCoordPix ./ focalLength;
ratioMeasurement = measurementCoordPix ./ focalLength;

% Compute the real world coords by scaling the triangle side ratio with the real, measured distance from the camera to the waypoint
goalCoordReal = ratioGoal .* principalDist;
measurementCoordReal = ratioMeasurement .* principalDist;

% Compute distances
distance = zeros(nImages, 2);
distance(:,1) = measurementCoordReal(:, 1) - goalCoordReal(1);
distance(:,2) = measurementCoordReal(:, 2) - goalCoordReal(2);

% Compute statistics
distanceMean = mean(distance)
distanceStdDeviation = std(distance)

% Compute total distances
totalDistance = zeros(nImages, 1);
for i=1:size(distance, 1)
    totalDistance(i) = sqrt(distance(i, 1)^2 + distance(i, 1)^2);
end

% Compute statistics for total distances
totalDistanceMean = mean(totalDistance)
totalDistanceStdDeviation = std(totalDistance)

% Save the results
save('results.mat');

%% Plot the data
clf
hold on
plot([-2, 2], [0, 0], 'k--')
plot([0, 0], [-2, 2], 'k--')
plot(distance(:, 1), distance(:, 2), '*')
xlim([-2 2])
ylim([-2 2])
xlabel('x error [m]')
ylabel('y error [m]')
title('Drone waypoint return error')
grid on