X = rand(3,3) + [zeros(2,3);ones(1,3)]*5;
C = rand(3,1)*2;
P = [eye(3),-C]; 

% Project points 
x = P*[X;ones(1,3)];
for i = 1:3
    x(:,i) = x(:,i)/x(3,i);
end

x = normc(x);