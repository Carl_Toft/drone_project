#ifndef _CAMERA_POSE_H
#define _CAMERA_POSE_H

#include <Eigen/Eigen>

Eigen::MatrixXd cameraPoseRANSAC(const Eigen::MatrixXd& impoints, const Eigen::MatrixXd& spacePoints, unsigned int numIterations, double tol, unsigned int& oNumInliers);

#endif