/* \author Geoffrey Biggs */
#include <iostream>
#include <fstream>
#include <sstream>
#include <boost/thread/thread.hpp>
#include <pcl/common/common_headers.h>
#include <pcl/io/pcd_io.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/common/transforms.h>

int tsvToMatrix (std::string fileName, Eigen::MatrixXf& matrix, const int nRows)
{
    std::ifstream inFile;
    std::string line;
    std::string entry;
    const char delim = '\t';
    
    std::vector<float> matrix_vec;
    
    int nCols = 0;
    
    // Read the values into a vector
    inFile.open(fileName.c_str());
    if(!inFile.is_open()) return 1;
    while (getline(inFile, line)){
        std::stringstream strStream(line);
        while(getline(strStream, entry, delim)){
//             matrix_vec.push_back( std::stod(entry) );
            matrix_vec.push_back( atof(entry.c_str()) );
        }
    }
    inFile.close();

    nCols = matrix_vec.size() / nRows;
    
    // OBS Reading straight from file should render row major order! Fix this with stride or transpose!
    
    // Mapping the vectors to matrices
    matrix = Eigen::MatrixXf::Map(&matrix_vec[0], nCols, nRows); // OBS dimensions swapped
    
    // Transposing the matrices to get back to column major order
    matrix.transposeInPlace();
    
    return 0;
}

void readPos (std::string filename, pcl::PointXYZ& point, pcl::PointXYZ& dir, float scaling_factor){
    std::string line;
    std::ifstream file (filename.c_str());
    if(!file.is_open())
        return;
    getline(file, line);
    file.close();
    std::stringstream stream(line);
    stream >> point.x;
    stream >> point.y;
    stream >> point.z;
    stream >> dir.x;
    stream >> dir.y;
    stream >> dir.z;
    point.x *= scaling_factor;
    point.y *= scaling_factor;
    point.z *= scaling_factor;
}

void updateCone (boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer, const std::string & shape_id, pcl::ModelCoefficients & coeffs, pcl::PointXYZ& point, pcl::PointXYZ& dir, float scaling_factor)
{
    float length = 0.4;// * scaling_factor;
    // tip coord
    coeffs.values[0] = point.x;
    coeffs.values[1] = point.y;
    coeffs.values[2] = point.z;
    // central axis vector (also length, pointing tip to base)
    coeffs.values[3] = dir.x * length;
    coeffs.values[4] = dir.y * length;
    coeffs.values[5] = dir.z * length;
    // opening angle in degrees
    coeffs.values[6] = 30.0;

    viewer->removeShape (shape_id, 0);
    viewer->addCone (coeffs, shape_id);
}

void colorMap (uint8_t& r_out, uint8_t& g_out, uint8_t& b_out, pcl::PointXYZ& point, pcl::PointXYZ& minPoint, pcl::PointXYZ& maxPoint)
{
    float scaleLength = maxPoint.y-minPoint.y;
    scaleLength *= 0.9;
    float value = point.y;
    value -= minPoint.y; // translate so that the scale starts at zero
    float x = value / scaleLength;
    
    float r, g, b;
    if (x < 0.5)
    {
        r = 1.0-2.0*x;
        g = 2.0*x;
        b = 0.0;
    }
    else
    {
        r = 0.0;
        g = 1.0-2.0*x;
        b = 2.0*x;
    }
    
    r *= 255;
    g *= 255;
    b *= 255;
    
    r_out = (uint8_t) r;
    g_out = (uint8_t) g;
    b_out = (uint8_t) b;
}

void colorMap2 (uint8_t& r_out, uint8_t& g_out, uint8_t& b_out, int index, Eigen::MatrixXf& colors)
{
    r_out = (uint8_t) colors(0, index);
    g_out = (uint8_t) colors(1, index);
    b_out = (uint8_t) colors(2, index);
}

// --------------
// -----Main-----
// --------------
int
main (int argc, char** argv)
{
    std::string dataDir = "../../bebopAutoFly/data/";
    std::string fileNameCloud = "coordCloud.pcd";
    std::string fileNameDronePos = "dronePos.tsv";
    std::string fileNameColorCloud = "colorCloud.tsv";
    std::string fileNameTransform = "transformation.tsv";
    float scaling_factor = 100;//2;//38.936;
    
    // ------------------------------------
    // -----Load point cloud from file-----
    // ------------------------------------
    pcl::PointCloud<pcl::PointXYZ>::Ptr initial_cloud (new pcl::PointCloud<pcl::PointXYZ>);
    if (pcl::io::loadPCDFile<pcl::PointXYZ> (dataDir + fileNameCloud, *initial_cloud) == -1)
    {
        PCL_ERROR ("Couldn't read file test_pcd.pcd \n");
        return (-1);
    }
    
    // ------------------------------------
    // -----Manipulate cloud---------------
    // ------------------------------------
    // Scale up the size of the cloud
    Eigen::MatrixXf transform = Eigen::Matrix4f::Identity();
    tsvToMatrix (dataDir + fileNameTransform, transform, 4);
    transform *= scaling_factor;
    pcl::PointCloud<pcl::PointXYZ>::Ptr basic_cloud (new pcl::PointCloud<pcl::PointXYZ> ());
    pcl::transformPointCloud (*initial_cloud, *basic_cloud, transform);
    
    // Fix RGB field
//     pcl::PointXYZ minPoint;
//     pcl::PointXYZ maxPoint;
//     pcl::getMinMax3D (*basic_cloud, minPoint, maxPoint);
    Eigen::MatrixXf colors;
    tsvToMatrix (dataDir + fileNameColorCloud, colors, 3);
    
    pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud (new pcl::PointCloud<pcl::PointXYZRGB>);
    uint8_t r, g, b;
    for (int i=0; i<basic_cloud->size(); i++)
    {
        pcl::PointXYZRGB point;
        point.x = basic_cloud->at(i).x;
        point.y = basic_cloud->at(i).y;
        point.z = basic_cloud->at(i).z;
//         colorMap (r, g, b, basic_cloud->at(i), minPoint, maxPoint);
        colorMap2 (r, g, b, i, colors);
        uint32_t rgb = (static_cast<uint32_t>(r) << 16 |
                        static_cast<uint32_t>(g) << 8  |
                        static_cast<uint32_t>(b));
        point.rgb = *reinterpret_cast<float*>(&rgb);
        cloud->points.push_back (point);
    }
    
    cloud->width = (int) cloud->points.size ();
    cloud->height = 1;
    
    // --------------------------------------------
    // -----Open 3D viewer and add point cloud-----
    // --------------------------------------------
    boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer (new pcl::visualization::PCLVisualizer ("3D Viewer"));
    viewer->setBackgroundColor (0, 0, 0);
    pcl::visualization::PointCloudColorHandlerRGBField<pcl::PointXYZRGB> rgb(cloud);
    viewer->addPointCloud<pcl::PointXYZRGB> (cloud, rgb, "sample cloud");
    viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 3, "sample cloud");
    viewer->addCoordinateSystem (1.0);
    viewer->initCameraParameters ();
    
//     boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer (new pcl::visualization::PCLVisualizer ("3D Viewer"));
//     viewer->setBackgroundColor (0, 0, 0);
//     viewer->addPointCloud<pcl::PointXYZ> (basic_cloud, "sample cloud");
//     viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 1, "sample cloud");
//     viewer->addCoordinateSystem (1.0);
//     viewer->initCameraParameters ();
    
    //---------------------------------------
    //-----Add shapes------------------------
    //---------------------------------------
    const std::string shape_id = "cone";
    pcl::ModelCoefficients coeffs;
    pcl::PointXYZ shapePosPoint;
    pcl::PointXYZ shapePosDir;
    coeffs.values.push_back (0.0); // tip coord
    coeffs.values.push_back (0.0);
    coeffs.values.push_back (0.0);
    coeffs.values.push_back (0.2); // central axis vector (also length, pointing tip to base)
    coeffs.values.push_back (0.2);
    coeffs.values.push_back (0.2);
    coeffs.values.push_back (30.0); // opening angle in degrees
    viewer->addCone (coeffs, shape_id);
    
    //--------------------
    // -----Main loop-----
    //--------------------
    while (!viewer->wasStopped ())
    {
        viewer->spinOnce (100);
        //boost::this_thread::sleep (boost::posix_time::microseconds (100000));
        readPos (dataDir + fileNameDronePos, shapePosPoint, shapePosDir, scaling_factor);
        updateCone (viewer, shape_id, coeffs, shapePosPoint, shapePosDir, scaling_factor);
    }
}