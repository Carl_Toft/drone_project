#include <iostream>
#include <fstream>
#include <cmath>
#include <unistd.h>

using namespace std;

int main(){
    
    float x(0), y(1), z(0), t(0), dt(0.05);
    ofstream file;
    string filename = "../../bebopAutoFly/data/dronePos.tsv";
    
    for(int i=0; i<6000; i++){
        y = cos(t);
        z = sin(t);
        t += dt;
        file.open(filename);
        file << x << "\t" << y << "\t" << z << endl;
        file.close();
        usleep(100000);
    }
    
    return 0;
}