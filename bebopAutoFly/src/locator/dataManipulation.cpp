#include "getMatches.h"
#include <Eigen/Dense>

void normColumns(Eigen::MatrixXd& matrix){
    // Norm the columns of the matrix
    for(int i = 0; i < matrix.cols(); i++){
        double invnorm = 1/matrix.col(i).norm();
        matrix.col(i) = matrix.col(i) * invnorm;
    }
}