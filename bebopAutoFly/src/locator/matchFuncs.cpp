
#include "getMatches.h"
#include <Eigen/Dense>
#include <cmath>
#include <vector>
#include <limits>
#include <iostream> // DEBUG

extern "C"{
    #include <vl/kdtree.h>
}

void initializeKDTree(VlKDForest*& kdtree, Eigen::MatrixXd& data){
    
    int dim = data.rows();
    int N = data.cols();
    int nTrees = 12;
    int maxComp = 50;
    
    kdtree = vl_kdforest_new (VL_TYPE_DOUBLE, dim, nTrees, VlDistanceL2);
    vl_kdforest_build (kdtree, N, &data(0,0));
    vl_kdforest_set_max_num_comparisons(kdtree, maxComp);
}

struct IndexPair{
    int image;
    int cloud;
};

void naiveMatcher(VlKDForestNeighbor neighbors[2], Eigen::MatrixXd& data, Eigen::Matrix<double, Eigen::Dynamic, 1>& query){
    // A slow matcher used for debug purposes.
    neighbors[0].distance = std::numeric_limits<double>::infinity();
    neighbors[1].distance = std::numeric_limits<double>::infinity();
    neighbors[0].index= 0;
    neighbors[1].index= 0;
    double dist;
    
    for(int i = 0; i < data.cols(); i++){
        Eigen::Matrix<double, Eigen::Dynamic, 1> diff = data.col(i) - query;
        dist = diff.transpose() * diff;
        if (dist < neighbors[0].distance){
            neighbors[1].distance = neighbors[0].distance;
            neighbors[1].index = neighbors[0].index;
            neighbors[0].distance = dist;
            neighbors[0].index = i;
        } else if (dist < neighbors[1].distance){
            neighbors[1].distance = dist;
            neighbors[1].index = i;
        }
    }
}

void matchSiftPoints (VlKDForest*& kdtree, Eigen::MatrixXd& descImage, Eigen::MatrixXd& descCloud, Eigen::MatrixXd& coordImage_in, Eigen::MatrixXd& coordCloud_in, Eigen::MatrixXd& coordImage_out, Eigen::MatrixXd& coordCloud_out){
    const int nbrOfNeighbors = 2;
    const double distRatio = 0.65; // rejection criterion (std = 0.5)
    std::vector<struct IndexPair> matchedIndices;
    
    // Search for matches
    for (int i = 0; i < descImage.cols(); i++){
        VlKDForestNeighbor neighbors[nbrOfNeighbors];
        vl_kdforest_query (kdtree, neighbors, nbrOfNeighbors, &descImage(0,i) );
//         Eigen::Matrix<double, Eigen::Dynamic, 1> query = descImage.col(i);
//         naiveMatcher(neighbors, descCloud, query);
        
        double maxVals = descImage.col(i).transpose() * descCloud.col(neighbors[0].index);
        double secondMaxVals = descImage.col(i).transpose() * descCloud.col(neighbors[1].index);
    
        if ( acos(maxVals) < acos(secondMaxVals) * distRatio ){ // What we match is the angles between unit vectors in feature space
            struct IndexPair indexPair;
            indexPair.image = i;
            indexPair.cloud = neighbors[0].index;
            matchedIndices.push_back(indexPair);
        }
    }
    
    // Extract matrices of corresponding feature vectors from indices
    int n = matchedIndices.size();
    
    coordImage_out.resize(3, n);
    coordCloud_out.resize(4, n);
    
    for (int i=0; i<n; i++){
        coordImage_out.col(i) = coordImage_in.col(matchedIndices[i].image);
        coordCloud_out.col(i) = coordCloud_in.col(matchedIndices[i].cloud);
    }
    
}