
#include "getMatches.h"
#include <Eigen/Dense>
#include <vector>
#include <string>

extern "C"{
    #include <vl/generic.h>
    #include <vl/pgm.h>
    #include <vl/sift.h>
}

int getSiftPoints (std::string imageName, Eigen::MatrixXd& coordinates, Eigen::MatrixXd& descriptors){
    char * imageName_cstr = new char [imageName.length()+1];
    std::strcpy(imageName_cstr, imageName.c_str());
    
    int nOctaves = -1; // -1 -> maximum
    int lvlPerOctave = 3; // 3 = standard
    int firstOctave = 0;

    int first;
    
    std::vector<double> coordinates_vec;
    std::vector<double> descriptors_vec;
    
    int nCols = 0;
    int const nRowsCoord = 3;
    int const nRowsDesc = 128;
    
    VlPgmImage *imageHeader = (VlPgmImage*) malloc(sizeof(VlPgmImage));
    float *imageData = NULL;
    
    if( vl_pgm_read_new_f(imageName_cstr, imageHeader, &imageData) ) return 1 ;
    
    VlSiftFilt *siftFilter = vl_sift_new((int)(imageHeader->width), // width
                                         (int)(imageHeader->height), // height
                                         nOctaves, //# octaves (-1 -> maximum #)
                                         lvlPerOctave, // # levels per octave (default = 3)
                                         firstOctave); // first octave index (0 = start at full resolution)
    
    if (siftFilter == NULL){
        return 1;
    }
    
    first = 1;
    while (1){
        int                   err;
        VlSiftKeypoint const *keys  = NULL ;
        int                   nkeys = 0 ;
        
        /* Calculate the GSS for the next octave .................... */
        if (first) {
            err   = vl_sift_process_first_octave (siftFilter, imageData) ;
            first = 0 ;
        } else {
            err   = vl_sift_process_next_octave  (siftFilter) ; // OBS! Empties the internal keypoint buffer.
        }

        if (err) break ;
        
        /* run detector ............................................. */
        vl_sift_detect (siftFilter) ;
        keys  = vl_sift_get_keypoints  (siftFilter) ;
        nkeys = vl_sift_get_nkeypoints (siftFilter) ;        
        
        /* for each keypoint ........................................ */
        for (int ikey = 0; ikey < nkeys ; ++ikey) {
            double angles[4];
            int nangles;
            VlSiftKeypoint const *key;
            
            /* compute keypoint orientations ........................... */
            key = keys + ikey;
            nangles = vl_sift_calc_keypoint_orientations(siftFilter, angles, key);
            
            /* for each orientation ................................... */
            for (int iOrient = 0 ; iOrient < (unsigned) nangles ; ++iOrient) {
                vl_sift_pix descr [128] ;

                /* compute descriptor ................................. */
                vl_sift_calc_keypoint_descriptor(siftFilter, descr, key, angles [iOrient]) ;
                
                /* save keypoint with descriptor ...................... */
                nCols++;
                
                coordinates_vec.push_back( (double) key->x);
                coordinates_vec.push_back( (double) key->y);
                coordinates_vec.push_back( (double) 1.0);
                
                for (int l = 0 ; l < 128 ; ++l) {
                    double x = 512.0 * descr[l] ;
                    x = (x < 255.0) ? x : 255.0 ;
                    x = floor(x);
                    descriptors_vec.push_back( x );
                }
            }
        }

    }
    
    /* Reinterpret the vector-containters as Eigen-matrices ........... */
    coordinates = Eigen::MatrixXd::Map(&coordinates_vec[0], nRowsCoord, nCols);
    descriptors = Eigen::MatrixXd::Map(&descriptors_vec[0], nRowsDesc,  nCols);
    
    /* Clean up ...................................................... */
    vl_sift_delete(siftFilter);
    free(imageHeader);
    free(imageData);
    
    return 0;
}