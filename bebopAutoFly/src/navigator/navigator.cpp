
#include "navigator.h"
#include "misc.h"
#include "tsvToMatrix.h"
#include <Eigen/Geometry>
#include <cmath>
#include <string>
#include <iostream>

Navigator::Navigator(){
    (*this).droneController = NULL;
    locator = NULL;
    userInterface = NULL;
    shouldStop = true;
    threadCreated = false;
    schemeToStart = NAVIGATOR_SCHEME_NONE;
    sem_init(&sem_moveComplete, 0, 0); // TODO move to a setup-function
    dataDir = "data/";
    fileNameTransformation = "transformation.tsv";
    fileNameManualWaypoint = "waypoint.tsv";
    reportedMove.setZero();
}

void Navigator::close(){
    mainLoopRunning = false;
    shouldStop = true;
    if (threadCreated){
        pthread_join(thread, NULL);
    }
    sem_destroy(&sem_moveComplete);
}

void Navigator::stop(){
    shouldStop = true;
}

void Navigator::setDroneController(DroneController * d){
    (*this).droneController = d;
}

void Navigator::setLocator(Locator * l){
    locator = l;
}

void Navigator::setUserInterface(UserInterface * u){
    userInterface = u;
}

void Navigator::moveComplete(double dX, double dY, double dZ){
    reportedMove << dX, dY, dZ;
    sem_post(&sem_moveComplete);
}

int Navigator::waitForMoveComplete(){
    while(true){
        if(shouldStop || !mainLoopRunning){
            (*this).droneController->commandCallback(DRONE_COMMAND_STOP);
            shouldStop = true;
            return 1;
        }else if(!sem_trywait(&sem_moveComplete)){
            return 0;
        }
        usleep(100);
    }
}

int Navigator::startCalibrationDance(){
    if (threadCreated || (*this).droneController==NULL || locator==NULL){
        return 1;
    }else{
        schemeToStart = NAVIGATOR_SCHEME_CALIBRATION_DANCE_2;
        shouldStop = false;
        pthread_create(&thread, NULL, &thread_starter, (void *)this);
        threadCreated = true;
        return 0;
    }
}

int Navigator::startMoveToWaypoint(){
    if (threadCreated || (*this).droneController==NULL || locator==NULL){
        return 1;
    }else{
        schemeToStart = NAVIGATOR_SCHEME_MOVE_TO_WAYPOINT;
        shouldStop = false;
        pthread_create(&thread, NULL, &thread_starter, (void *)this);
        threadCreated = true;
        return 0;
    }
}

int Navigator::startSaveWaypoint(){
    if (threadCreated || (*this).droneController==NULL || locator==NULL){
        return 1;
    }else{
        schemeToStart = NAVIGATOR_SCHEME_SAVE_WAYPOINT;
        shouldStop = false;
        pthread_create(&thread, NULL, &thread_starter, (void *)this);
        threadCreated = true;
        return 0;
    }
}

void *Navigator::thread_starter(void *context){ // helper function to make a thread use a member-function
    Navigator * nav = (Navigator *) context;
    (*nav).userInterface->updateNavigationState((int)(*nav).schemeToStart);
    switch( (*nav).schemeToStart ){
        case NAVIGATOR_SCHEME_NONE:
            break;
        case NAVIGATOR_SCHEME_CALIBRATION_DANCE:
            nav->calibrationDance();
            break;
        case NAVIGATOR_SCHEME_MOVE_TO_WAYPOINT:
            nav->moveToWaypoint();
            break;
        case NAVIGATOR_SCHEME_SAVE_WAYPOINT:
            nav->saveWaypoint();
            break;
        case NAVIGATOR_SCHEME_CALIBRATION_DANCE_2:
            nav->calibrationDance2();
            break;
        default:
            break;
    }
    (*nav).schemeToStart = NAVIGATOR_SCHEME_NONE;
    (*nav).userInterface->updateNavigationState((int)(*nav).schemeToStart);
    (*nav).threadCreated = false;
    pthread_exit(NULL);
}

void Navigator::saveWaypoint(){
    Pose pose;
    locator->stopContinuousImageLocalization_blocking();
    locator->startOneShotImageLocalization(pose);
    Eigen::MatrixXd m = pose.cameraMatrix;
    matrixToTsv (dataDir+fileNameManualWaypoint, m);
}

void Navigator::moveToWaypoint(){
    /*Eigen::Vector3d path_droneFrame,
                    path_cameraFrame,
                    path_cloudFrame;
    Eigen::Matrix3d cloudToCameraFrame,
                    cameraToDroneFrame;
    Pose pose;
    locator->startOneShotImageLocalization(pose);
    cloudToCameraFrame = pose.cameraMatrix.block<3,3>(0,0);
    cameraToDroneFrame << 1, 0, 0,
                          0, 0, 1,
                          0,-1, 0;
    path_cloudFrame = pose.cameraMatrix.block<1,3>(0,0);
    path_cameraFrame = cloudToCameraFrame * path_cloudFrame;
    path_droneFrame  = cameraToDroneFrame * path_cameraFrame;
    (*this).droneController->commandCallback(DRONE_COMMAND_MOVE_BY, path_droneFrame(0), path_droneFrame(1), path_droneFrame(2));*/
    /*Pose pose, waypoint;
    Eigen::MatrixXd waypoint_cameraPose;
    double angle;
    locator->startOneShotImageLocalization(pose);
    tsvToMatrix (dataDir+fileNameManualWaypoint, waypoint_cameraPose, 3);
    waypoint.updateWithCameraPose(waypoint_cameraPose);
    angle = rotationAnglePlaneProj(pose, waypoint.heading);
    std::cout << "angle (deg): " << angle*180/3.1415 << std::endl;
    (*this).droneController->commandCallback(DRONE_COMMAND_TURN_BY, angle);*/
    const double    waypointArrivalDistanceCriterion = 0.5, // meters
                    jumpMaxDist = 0.5; // meters
    double          angle,
                    scale;
//     bool            firstJump = true;
    Eigen::Matrix3d cloudToCameraFrame,
                    cameraToDroneFrame;
    Eigen::MatrixXd waypoint_cameraPose;
    Eigen::Vector3d path_cloudFrame,
                    path_droneFrame;
    Pose waypoint,
         pose,
         previousPose;
    cameraToDroneFrame << 1, 0, 0,
                          0, 0, 1,
                          0,-1, 0;
    locator->stopContinuousImageLocalization_blocking();
    tsvToMatrix (dataDir+fileNameManualWaypoint, waypoint_cameraPose, 3);
    waypoint.updateWithCameraPose(waypoint_cameraPose);
    while(true){
        (*this).droneController->commandCallback(DRONE_COMMAND_STOP);
//         previousPose.updatePose(pose);
        locator->startOneShotImageLocalization(pose);
//         if( firstJump ){
//             firstJump = false;
//         }else{
//             scale = (pose.position - previousPose.position).norm() / reportedMove.norm();
//             locator->setScale(scale);
//         }
        cloudToCameraFrame = pose.cameraMatrix.block<3,3>(0,0); // Scale is not included in this transform, it is handled later.
        path_cloudFrame  = waypoint.position - pose.position; // vector from pose to waypoint
        path_droneFrame  = cameraToDroneFrame * cloudToCameraFrame * path_cloudFrame / locator->getScale(); // 0.00190001
        std::cout << "dist\t" << path_droneFrame.norm() << std::endl; // DEBUG
        if ( path_droneFrame.norm() < waypointArrivalDistanceCriterion ){
            std::cout << "arrived" << std::endl;
            break;
        }
        if ( path_droneFrame.norm() > jumpMaxDist ){
            path_droneFrame.normalize();
            path_droneFrame *= jumpMaxDist;
        }
        std::cout << "path" << std::endl << path_droneFrame << std::endl; // DEBUG
        (*this).droneController->commandCallback(DRONE_COMMAND_MOVE_BY, path_droneFrame(0), path_droneFrame(1), path_droneFrame(2));
        if ( waitForMoveComplete() ) { return; }
    }
    locator->startOneShotImageLocalization(pose);
    angle = rotationAnglePlaneProj(pose, waypoint.heading);
    (*this).droneController->commandCallback(DRONE_COMMAND_TURN_BY, angle);
    if ( waitForMoveComplete() ) { return; }
}

double Navigator::rotationAnglePlaneProj(Pose pose, Eigen::Vector3d vec){
    Eigen::Vector3d right, forward, up, upProj;
    double angle_right, angle_forward, sign;
    right   =  pose.cameraMatrix.block<1,3>(0,0);
    forward =  pose.cameraMatrix.block<1,3>(2,0);
    up      = -pose.cameraMatrix.block<1,3>(1,0);
    upProj  = vec.dot(up) * up;
    vec    -= upProj;
    angle_forward = acos(forward.dot(vec)/forward.norm()/vec.norm()); // rotation angle
    angle_right   = acos(right.dot(vec)/right.norm()/vec.norm());
    if ( angle_right < 3.1415/2.0 ){
        sign = -1;
    }else{
        sign = 1;
    }
    return angle_forward * sign;
}

void Navigator::calibrationDance2(){
    Pose pose1, pose2;
    locator->stopContinuousImageLocalization_blocking();
    locator->startOneShotImageLocalization(pose1);
    (*this).droneController->commandCallback(DRONE_COMMAND_MOVE_BY, 1, 0, 0);
    if ( waitForMoveComplete() ) { return; }
    locator->startOneShotImageLocalization(pose2);
    double scale = (pose2.position - pose1.position).norm() / reportedMove.norm();
    std::cout << "real length :" << reportedMove.norm() << std::endl;
    std::cout << "cloud length :" << (pose2.position - pose1.position).norm() << std::endl;
    std::cout << "scale :" << scale << std::endl;
    locator->setScale(scale);
    std::cout << "new scale :" << locator->getScale() << std::endl;
}

void Navigator::calibrationDance(){
    locator->stopContinuousImageLocalization_blocking();
    
    Eigen::Vector3d v1, v2, u1, u2;
    Eigen::Matrix4d T1, T2;
    double flightDist1 = 1.0; // meters
    double flightDist2 = 1.0; // meters
    
    Eigen::Vector3d move1(0, 0, flightDist1); // upward
    Eigen::Vector3d move2(flightDist2, 0, 0); // right
    
    measureVector(u1, v1, move1);
    if (shouldStop) { return; }
    T1 = getTransformationMatrix(u1, v1);
    locator->updateCloud(T1);
//     std::cout << "DEBUG, u1: " << std::endl << u1 << std::endl;
//     std::cout << "DEBUG, v1: " << std::endl << v1 << std::endl;
//     std::cout << "DEBUG, T1: " << std::endl << T1 << std::endl;
    
    measureVector(u2, v2, move2);
    if (shouldStop) { return; }
//     u2(2) = 0; // projecting the vector into the xy-plane
//     v2(2) = 0; // projecting the vector into the xy-plane
//     u2.normalize();
//     v2.normalize();
    T2 = getTransformationMatrix(u2, v2);
    locator->updateCloud(T2);
    
    Eigen::MatrixXd T = T2 * T1;
//     Eigen::MatrixXd T = T1;
    matrixToTsv (dataDir+fileNameTransformation, T);
}

void Navigator::measureVector(Eigen::Vector3d& u_image, Eigen::Vector3d& v_imu, Eigen::Vector3d& move){
    Pose p1, p2, v1, v2;
    locator->getPoseIMU(v1);
    locator->startOneShotImageLocalization(p1);
    (*this).droneController->commandCallback(DRONE_COMMAND_MOVE_BY, move(0), move(1), move(2));
    if ( waitForMoveComplete() ) { return; }
    locator->getPoseIMU(v2);
    locator->startOneShotImageLocalization(p2);
    u_image = p2.position - p1.position;
    v_imu   = v2.position - v1.position;
}

Eigen::Matrix4d Navigator::getTransformationMatrix(Eigen::Vector3d& v_start, Eigen::Vector3d& v_goal){
    Eigen::Matrix3d crossMatrix, R;
    Eigen::Matrix4d T;
    Eigen::Vector3d u;
    double angle, scale;
    u = v_start.cross(v_goal); // rotation axis
    u.normalize();
    angle = acos(v_start.dot(v_goal)/v_start.norm()/v_goal.norm()); // rotation angle
    crossMatrix <<   0   , -u(2),  u(1),
                     u(2),  0   , -u(0),
                    -u(1),  u(0),  0   ;
    R = Eigen::MatrixXd::Identity(3,3)*cos(angle) + crossMatrix*sin(angle) + (1-cos(angle))*u*u.transpose(); // rotation matrix
    scale = v_goal.norm()/v_start.norm(); // scale factor
    T.setZero();
    T.topLeftCorner(3,3) = R;
    T(3,3) = 1;
    T *= scale;
    return T;
}